#!/bin/bash
# create a directory to install to
INSTALL_PATH=$(pwd)/local
mkdir -p $INSTALL_PATH

# clone libzmq source code
TAG="v4.3.2"
URL="https://github.com/zeromq/libzmq.git"
git clone --depth 1 --branch $TAG $URL

# build and install libzmq compiler locally
pushd libzmq
	./autogen.sh
	./configure --prefix=$INSTALL_PATH --enable-static
	make -j$(nproc)
	make install
popd

# clean up source tree
rm -rf libzmq
