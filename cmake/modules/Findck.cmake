#
# Find the Concurrency Kit includes and library
#
# Emitted targets: ck
find_path (
	${CMAKE_FIND_PACKAGE_NAME}_INCLUDE_DIRS
	NAMES ck_stdlib.h
)
find_library (
	${CMAKE_FIND_PACKAGE_NAME}_LIBRARIES
	NAMES libck ck
)

# Handle find_package's QUIET, REQUIRED etc. arguments
include(FindPackageHandleStandardArgs)

find_package_handle_standard_args(
	${CMAKE_FIND_PACKAGE_NAME}
	REQUIRED_VARS ${CMAKE_FIND_PACKAGE_NAME}_LIBRARIES ${CMAKE_FIND_PACKAGE_NAME}_INCLUDE_DIRS
)

# Add imported target to the CMake build
add_library(${CMAKE_FIND_PACKAGE_NAME} STATIC IMPORTED)
set_target_properties(${CMAKE_FIND_PACKAGE_NAME} PROPERTIES
	INTERFACE_INCLUDE_DIRECTORIES "${${CMAKE_FIND_PACKAGE_NAME}_INCLUDE_DIRS}"
	IMPORTED_LOCATION "${${CMAKE_FIND_PACKAGE_NAME}_LIBRARIES}"
)
