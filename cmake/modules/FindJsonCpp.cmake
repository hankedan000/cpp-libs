#
# Find the jsoncpp includes and library
#
# This module defines
# JsonCpp_INCLUDE_DIR, where to find argparse.h, etc.
# JsonCpp_LIBRARIES, the libraries to link against to use jsoncpp.
# JsonCpp_FOUND, If false, do not try to use jsoncpp.

# also defined, but not for general use are
# JsonCpp_LIBRARY, where to find the jsoncpp library.

find_path(JsonCpp_INCLUDE_DIR json/json.h
  /usr/local/include
  /usr/include
)

find_library(JsonCpp_LIBRARY libjsoncpp.a
  /usr/local/lib
  /usr/lib)

set(JsonCpp_FOUND "NO")
if (JsonCpp_INCLUDE_DIR)
  if (JsonCpp_LIBRARY)
    set(JsonCpp_FOUND "YES")
    set(JsonCpp_LIBRARIES ${JsonCpp_LIBRARY})
  endif()
endif()