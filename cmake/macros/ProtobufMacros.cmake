macro (get_proto_imports PROTO_FILE PROTO_IMPORTS)
	file(READ "${PROTO_FILE}" file_contents)
	STRING(REGEX REPLACE ";" "\\\\;" file_lines "${file_contents}")
	STRING(REGEX REPLACE "\n" ";" file_lines "${file_lines}")
	set(${PROTO_IMPORTS} "" PARENT_SCOPE)
	foreach (line ${file_lines})
		message(DEBUG "line = ${line}")
		string(REGEX MATCH "import *(public|weak)? *\"(.*)\"" _ ${line})
		message(DEBUG "CMAKE_MATCH_COUNT = ${CMAKE_MATCH_COUNT}")
		if (${CMAKE_MATCH_COUNT} EQUAL 2)
			list(APPEND ${PROTO_IMPORTS} ${CMAKE_MATCH_2})
		elseif (${CMAKE_MATCH_COUNT} EQUAL 1)
			list(APPEND ${PROTO_IMPORTS} ${CMAKE_MATCH_1})
		endif()
	endforeach()
endmacro()

macro (create_protobuf_target PROTO_FILE)
	message(DEBUG "create_protobuf_target(${PROTO_FILE})")

	# generate C++ source from proto source
	protobuf_generate_cpp(PROTO_SRCS PROTO_HDRS ${PROTO_FILE})
	
	# create shared & static library targets for the protobuf file
	get_filename_component(PROTO_TARGET ${PROTO_FILE} NAME_WE)
	set(PROTO_TARGET_SHARED "${PROTO_TARGET}_pb")
	set(PROTO_TARGET_STATIC "${PROTO_TARGET}_pb_static")
	add_library(${PROTO_TARGET_SHARED} SHARED ${PROTO_SRCS})
	add_library(${PROTO_TARGET_STATIC} STATIC ${PROTO_SRCS})
	target_link_libraries(${PROTO_TARGET_SHARED} ${Protobuf_LIBRARIES})
	target_link_libraries(${PROTO_TARGET_STATIC} ${Protobuf_LIBRARIES})
	target_include_directories(${PROTO_TARGET_SHARED} PUBLIC
		$<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>
		$<INSTALL_INTERFACE:include/${CMAKE_PROJECT_NAME}>)
	target_include_directories(${PROTO_TARGET_STATIC} PUBLIC
		$<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>
		$<INSTALL_INTERFACE:include/${CMAKE_PROJECT_NAME}>)
	set_target_properties(${PROTO_TARGET_SHARED} PROPERTIES PUBLIC_HEADER ${PROTO_HDRS})
	set_target_properties(${PROTO_TARGET_STATIC} PROPERTIES PUBLIC_HEADER ${PROTO_HDRS})

	# find and link all the imported .proto targets to this target
	get_proto_imports(${PROTO_FILE} proto_imports)
	foreach (import ${proto_imports})
		get_filename_component(proto_dep ${import} NAME_WE)
		set(proto_dep_shared "${proto_dep}_pb")
		set(proto_dep_static "${proto_dep}_pb_static")
		target_link_libraries(${PROTO_TARGET_SHARED} ${proto_dep_shared})
		target_link_libraries(${PROTO_TARGET_STATIC} ${proto_dep_static})
	endforeach()
endmacro()
