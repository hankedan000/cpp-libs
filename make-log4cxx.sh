#!/bin/bash
# create a directory to install to
INSTALL_PATH=$(pwd)/local
mkdir -p $INSTALL_PATH

TAG="v0.11.0"
URL="https://github.com/apache/logging-log4cxx.git"
git clone --depth 1 --branch $TAG $URL

# build and install libzmq compiler locally
pushd logging-log4cxx
	mkdir build
	cd build
	cmake -DCMAKE_INSTALL_PREFIX:PATH=$INSTALL_PATH ..
	make -j$(nproc)
	make install
popd

# clean up source tree
rm -rf logging-log4cxx
