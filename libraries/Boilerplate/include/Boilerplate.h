#pragma once

#include "LoggingObject.h"
#include <string>
#include <unistd.h>

class Boilerplate : public LoggingObject
{
public:
	Boilerplate(
		const std::string &processName);

	~Boilerplate();

	pid_t
	fork(
		bool exitOnFail);

	pid_t
	daemonize();

private:
	// prevent default construction
	Boilerplate() = delete;

	// prevent copying
	Boilerplate(
		const Boilerplate &other) = delete;

};
