#pragma once

#include "Boilerplate.h"
#include "Reactor.h"
#include <unordered_map>
#include <zmq.h>

#define MAX_MONITOR_ENDPOINT_SIZE 1024

class ZMQ_Boilerplate : public Boilerplate
{
private:
	struct SocketInfo
	{
		/**
		 * ZMQ_PUB, ZMQ_SUB, ZMQ_PAIR etc.
		 */
		int type = -1;

		/**
		 * The endpoint of the ZMQ_PAIR socket used to monitor event for.
		 * The format of this string is "inproc://monitor-%p" where %p is the
		 * address of the socket pointer.
		 */
		char mon_endpoint[MAX_MONITOR_ENDPOINT_SIZE];

		/**
		 * the ZMQ_PAIR socket used for monitoring internal ZMQ events.
		 * created with create_monitor_socket()
		 */
		void *mon_socket = nullptr;
	};

public:
	/**
	 * Constructor for the ZMQ boilerplate
	 *
	 * @param[in] processName
	 * The process name that is creating/using this boilerplate.
	 * This string will be used to initialize the root logger
	 * name for this class.
	 */
	ZMQ_Boilerplate(
		const std::string &processName);

	~ZMQ_Boilerplate();

	/**
	 * Acessor for the ZMQ context
	 *
	 * @return
	 * the ZMQ context
	 */
	void *
	context() const;

	/**
	 * Acessor for the process's reactor class
	 *
	 * @return
	 * the reactor
	 */
	Reactor &
	reactor();

	/**
	 * Wrapper around the zmq_socket() call. This method exists
	 * so that the boilerplate can track and cleanup all the
	 * created sockets for this process.
	 *
	 * @return
	 * same return value as zmq_socket() documentation
	 */
	void *
	create_socket(
		int type);

	/**
	 * Creates and connects a ZMQ_PAIR socket for monitoring events
	 * on another socket (see zmq_socket_monitor() documentation)
	 *
	 * @param[in] socket_to_monitor
	 * the zmq socket you wish to monitor events on
	 *
	 * @param[out] socket
	 * the created socket. zmq library will send messages on this ZMQ_PAIR
	 * socket anytime an enabled event in 'events' occurs on 'socket_to_monitor'
	 * 
	 * @param[in] events
	 * see zmq_socket_monitor() documentation
	 * 
	 * @return
	 * 0 on success, -1 on error
	 */
	int
	create_monitor_socket(
		void *socket_to_monitor,
		void *&socket,
		int events = ZMQ_EVENT_ALL);

	/**
	 * Closes a socket that was previous created with create_socket()
	 *
	 * @param[in] socket
	 * the socket to close
	 *
	 * @return
	 * True on success, false if the socket was never created, or if
	 * zmq_close() call failed
	 */
	bool
	close_socket(
		void *socket);

private:
	bool
	internal_socket_close(
		std::unordered_map<void *, SocketInfo>::iterator sock_info_itr);

private:
	// the ZMQ context owned and managed by this class
	void *context_;

	// a set of all the opened sockets managed by this class
	std::unordered_map<void *, SocketInfo> socket_infos_;

	Reactor reactor_;

};