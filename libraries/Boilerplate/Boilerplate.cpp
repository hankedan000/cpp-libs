#include "Boilerplate.h"
#include "Logging.h"
#include <string.h>

Boilerplate::Boilerplate(
	const std::string &processName)
 : LoggingObject(processName)
{
}

Boilerplate::~Boilerplate()
{
}

pid_t
Boilerplate::fork(
	bool exitOnFail)
{
	pid_t pid = 0;

	// perform fork and reinit the logging
	Logging::before_fork();
	pid = ::fork();
	Logging::after_fork();

	// log if error occurred
	if (pid < 0)
	{
		LOG4CXX_ERROR(getLogger(),
			__func__ << " - "
			"fork() failed. errno = " << strerror(errno) << " (" << errno << "); "
			"exitOnFail = " << std::boolalpha << exitOnFail);

		if (exitOnFail)
		{
			exit(EXIT_FAILURE);
		}
	}

	return pid;
}

pid_t
Boilerplate::daemonize()
{
	pid_t pid = this->fork(true);

	if (pid > 0)
	{// parent process
		exit(EXIT_SUCCESS);
	}
	else if (pid == 0)
	{// child process
		// On success: The child process becomes session leader */
		if (setsid() < 0)
		{
			LOG4CXX_ERROR(getLogger(),
				__func__ << " - "
				"setsid() failed. Failed to set child as session leader."
				" errno = " << strerror(errno) << " (" << errno << ")");
			exit(EXIT_FAILURE);
		}
	}
	else
	{
		// fork() exits on failure so no need to catch errors
	}

	return pid;
}