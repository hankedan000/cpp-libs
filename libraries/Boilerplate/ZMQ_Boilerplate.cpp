#include "ZMQ_Boilerplate.h"

ZMQ_Boilerplate::ZMQ_Boilerplate(
	const std::string &processName)
 : Boilerplate(processName)
 , context_(nullptr)
{
	context_ = zmq_ctx_new();
	reactor_.setLoggerPrefix(getLoggerNameFull());
}

ZMQ_Boilerplate::~ZMQ_Boilerplate()
{
	// close all opened sockets
	for (auto itr=socket_infos_.begin(); itr!=socket_infos_.end(); itr++)
	{
		internal_socket_close(itr);
	}

	// close the context
	if (context_)
	{
		if (zmq_ctx_destroy(context_) < 0)
		{
			LOG4CXX_ERROR(getLogger(),
				__func__ << " - "
				"zmq_ctx_destroy() failed! error = " << zmq_strerror(errno));
		}
		context_ = nullptr;
	}
}

void *
ZMQ_Boilerplate::context() const
{
	return context_;
}

Reactor &
ZMQ_Boilerplate::reactor()
{
	return reactor_;
}

void *
ZMQ_Boilerplate::create_socket(
	int type)
{
	void *new_socket = zmq_socket(context_,type);
	if (new_socket != nullptr)
	{
		SocketInfo new_info;
		new_info.type = type;
		socket_infos_.insert({new_socket,new_info});
	}
	else
	{
		LOG4CXX_ERROR(getLogger(),
			__func__ << " - "
			"failed to create socket of type " << type << "!"
			" error = " << zmq_strerror(errno));
	}
	return new_socket;
}

int
ZMQ_Boilerplate::create_monitor_socket(
	void *socket_to_monitor,
	void *&socket,
	int events)
{
	if (socket_to_monitor == nullptr)
	{
		LOG4CXX_ERROR(getLogger(),
			__func__ << " - "
			"socket_to_monitor is null!");
		return -1;
	}

	auto info_itr = socket_infos_.find(socket_to_monitor);
	if (info_itr == socket_infos_.end())
	{
		LOG4CXX_ERROR(getLogger(),
			__func__ << " - "
			"can only monitor events on sockets that were created with this boilerplate"
			" instance.");
		return -1;
	}

	// create the monitor socket
	auto &sock_info = info_itr->second;
	sprintf(sock_info.mon_endpoint, "inproc://monitor-%p", socket_to_monitor);
	int rc = zmq_socket_monitor(socket_to_monitor, sock_info.mon_endpoint, events);
	if (rc < 0)
	{
		LOG4CXX_ERROR(getLogger(),
			__func__ << " - "
			"zmq_socket_monitor() failed! "
			"error: " << zmq_strerror(errno) << "; "
			"mon_endpoint: " << sock_info.mon_endpoint << "; ");
		return -1;
	}
	socket = zmq_socket(context_, ZMQ_PAIR);
	rc = zmq_connect(socket, sock_info.mon_endpoint);
	if (rc < 0)
	{
		LOG4CXX_ERROR(getLogger(),
			__func__ << " - "
			"zmq_connect() failed! "
			"error: " << zmq_strerror(errno) << "; "
			"mon_endpoint: " << sock_info.mon_endpoint << "; ");
		zmq_close(socket);
		return -1;
	}

	sock_info.mon_socket = socket;
	return 0;
}

bool
ZMQ_Boilerplate::close_socket(
	void *socket)
{
	bool okay = true;

	auto itr = socket_infos_.find(socket);
	if (itr != socket_infos_.end())
	{
		if (internal_socket_close(itr))
		{
			socket_infos_.erase(itr);
		}
		else
		{
			okay = false;
		}
	}
	else
	{
		LOG4CXX_ERROR(getLogger(),
			__func__ << " - "
			"Requested close on unknown socket " << socket << "."
			" Make sure the socket has previous been created with"
			" ZMQ_Boilerplate::create_socket()");
		okay = false;
	}

	return okay;
}

bool
ZMQ_Boilerplate::internal_socket_close(
	std::unordered_map<void *, SocketInfo>::iterator sock_info_itr)
{
	bool okay = false;

	void *socket = sock_info_itr->first;
	auto &sock_info = sock_info_itr->second;

	// close any opened monitor sockets or else zmq_ctx_destroy() will hang
	if (sock_info.mon_socket != nullptr)
	{
		zmq_socket_monitor(socket, NULL, 0);
		if (zmq_disconnect(sock_info.mon_socket, sock_info.mon_endpoint) < 0)
		{
			LOG4CXX_ERROR(getLogger(),
				"zmq_disconnect() failed! "
				"error: " << zmq_strerror(errno) << "; "
				"mon_endpoint: " << sock_info.mon_endpoint << "; ");
			okay = false;
		}
		if (zmq_close(sock_info.mon_socket) < 0)
		{
			LOG4CXX_ERROR(getLogger(),
				"zmq_close() failed! "
				"error: " << zmq_strerror(errno));
			okay = false;
		}
		sock_info.mon_socket = nullptr;
	}

	// close the original socket now
	if (zmq_close(socket) < 0)
	{
		LOG4CXX_ERROR(getLogger(),
			__func__ << " - "
			"zmq_close() failed for socket " << socket << "!"
			" error = " << zmq_strerror(errno));
		okay = false;
	}

	return okay;
}
