#include <thread>
#include "Logging.h"
#include "SimpleMessage.pb.h"
#include "ZMQ_Boilerplate.h"

ZMQ_Boilerplate boilerplate("ReactorDemo");
bool stay_alive = true;

void
on_subscriber1_monitor_event(
	uint16_t event,
	const char *address)
{
	LOG4CXX_INFO(boilerplate.getLogger(),
		__func__ << " - " <<
		"address: " << address << "; "
		"event: " << zmq_event_str(event) << "; ");
}

void
on_subscriber2_monitor_event(
	uint16_t event,
	const char *address)
{
	LOG4CXX_INFO(boilerplate.getLogger(),
		__func__ << " - " <<
		"address: " << address << "; "
		"event: " << zmq_event_str(event) << "; ");
}

void
on_simple_message(
	const SimpleMessage &datum)
{
	LOG4CXX_INFO(boilerplate.getLogger(),
		"Received a SimpleMessage!\n" << datum.DebugString());
}

void
on_infinite_timer(
	int timer_id,
	void *vargs)
{
	LOG4CXX_INFO(boilerplate.getLogger(),
		"infinite timer fired!");
}

void
on_finite_timer(
	int timer_id,
	void *vargs)
{
	auto msg = (const char *)(vargs);
	LOG4CXX_INFO(boilerplate.getLogger(),
		"finite timer fired! msg = \"" << msg << "\"");
}

bool
publish_msg(
	void *pub_socket,
	const SimpleMessage &msg)
{
	bool okay = true;

	uint8_t buffer[10000];
	msg.SerializeToArray((void*)buffer,sizeof(buffer));
	size_t samp_size = msg.ByteSizeLong();

	if (zmq_send(pub_socket,(void*)buffer,samp_size,ZMQ_DONTWAIT) < 0)
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			"zmq_send failed for message '" << msg.DebugString() << "'");
		okay = false;
	}

	return okay;
}

// child process is spawned into this method
void publisher1()
{
	void *context = zmq_ctx_new();
	void *publisher = zmq_socket(context,ZMQ_PUB);
	const char *bind_host = "tcp://*:5557";
	LOG4CXX_INFO(boilerplate.getLogger(),
		__func__ << " - " <<
		"publisher1 binding to " << bind_host);
	int rc = zmq_bind(publisher,bind_host);

	SimpleMessage msg;
	msg.set_mystring("Helloworld! I'm publisher1.");
	unsigned int count = 0;
	while (stay_alive)
	{
		msg.set_myint(count++);
		publish_msg(publisher,msg);
		
		sleep(1);
	}

	zmq_close(publisher);
	zmq_ctx_destroy(context);

	LOG4CXX_DEBUG(boilerplate.getLogger(),
		"publisher1 stopped.");
}

// child process is spawned into this method
void publisher2()
{
	void *context = zmq_ctx_new();
	void *publisher = zmq_socket(context,ZMQ_PUB);
	const char *bind_host = "tcp://*:5558";
	LOG4CXX_INFO(boilerplate.getLogger(),
		__func__ << " - " <<
		"publisher2 binding to " << bind_host);
	int rc = zmq_bind(publisher,bind_host);

	SimpleMessage msg;
	msg.set_mystring("Helloworld! I'm publisher2.");
	unsigned int count = 0;
	while (stay_alive)
	{
		msg.set_myint(count++);
		publish_msg(publisher,msg);

		sleep(2);
	}

	zmq_close(publisher);
	zmq_ctx_destroy(context);

	LOG4CXX_DEBUG(boilerplate.getLogger(),
		"publisher2 stopped.");
}

int main()
{
	bool okay = true;
	auto &reactor = boilerplate.reactor();

	if ( ! Logging::was_init_from_config())
	{
		boilerplate.getLogger()->setLevel(log4cxx::Level::getInfo());
	}

	// spawn some publisher threads
	std::thread pub1(publisher1);
	std::thread pub2(publisher2);

	void *sub1 = boilerplate.create_socket(ZMQ_SUB);
	void *sub2 = boilerplate.create_socket(ZMQ_SUB);
	if (okay && zmq_connect(sub1,"tcp://localhost:5557") < 0)
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			"Failed to connect to subscriber!");
		okay = false;
	}
	if (okay && zmq_connect(sub2,"tcp://localhost:5558") < 0)
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			"Failed to connect to subscriber!");
		okay = false;
	}
	zmq_setsockopt(sub1,ZMQ_SUBSCRIBE,"",0);
	zmq_setsockopt(sub2,ZMQ_SUBSCRIBE,"",0);

	// setup socket monitoring
	void *sub1_mon;
	boilerplate.create_monitor_socket(sub1, sub1_mon);
	void *sub2_mon;
	boilerplate.create_monitor_socket(sub2, sub2_mon);

	okay = okay && reactor.add_pb_listener(sub1,on_simple_message);
	okay = okay && reactor.add_monitor_listener(sub1_mon,on_subscriber1_monitor_event);
	okay = okay && reactor.add_pb_listener(sub2,on_simple_message);
	okay = okay && reactor.add_monitor_listener(sub2_mon,on_subscriber2_monitor_event);
	int timer_id;
	okay = okay && reactor.add_timer_listener(4000,-1,timer_id,on_infinite_timer,nullptr);
	const char *msg = "Hello from finite timer!";
	okay = okay && reactor.add_timer_listener(10,5,timer_id,on_finite_timer,(void *)msg);
	if (okay)
	{
		LOG4CXX_INFO(boilerplate.getLogger(),
			"reactor is listening...");
		if ( ! reactor.start())
		{
			LOG4CXX_ERROR(boilerplate.getLogger(),
				"reactor failed to start!");
		}
		else
		{
			LOG4CXX_INFO(boilerplate.getLogger(),
				"reactor shutdown complete!");
		}
	}

	stay_alive = false;
	// wait for publishers to join
	pub1.join();
	pub2.join();

	return okay ? EXIT_SUCCESS : EXIT_FAILURE;
}