#pragma once

template <typename T, int capacity>
class RingFIFO
{
public:
    RingFIFO() : head(0), tail(0), count(0) {}
    bool isFull() const { return count == capacity; }
    bool isEmpty() const { return count == 0; }
    int size() const { return count; }
    void clear() { head = 0; tail = 0; count = 0; }
    void push(const T& value) {
        buffer[tail] = value;
        tail = (tail + 1) % capacity;
        if (isFull()) {
            head = (head + 1) % capacity;
        } else {
            ++count;
        }
    }
    T& front() { return buffer[head]; }
    const T& front() const { return buffer[head]; }
    void pop() {
        if (!isEmpty()) {
            head = (head + 1) % capacity;
            --count;
        }
    }
private:
    T buffer[capacity];
    int head, tail, count;
};
