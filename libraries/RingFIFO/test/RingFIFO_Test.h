#pragma once

#include "cppunit/ui/text/TestRunner.h"
#include "cppunit/TestFixture.h"
#include "cppunit/extensions/HelperMacros.h"

#include "RingFIFO.h"
#include <unistd.h>

class RingFIFO_Test : public CppUnit::TestFixture
{
	CPPUNIT_TEST_SUITE(RingFIFO_Test);
	CPPUNIT_TEST(testPushPop);
	CPPUNIT_TEST_SUITE_END();

public:
	void setUp();
	void tearDown();

protected:
	void testPushPop();

private:

};
