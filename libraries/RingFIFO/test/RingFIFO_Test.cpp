#include "RingFIFO_Test.h"

void
RingFIFO_Test::setUp()
{
}

void
RingFIFO_Test::tearDown()
{
}

void
RingFIFO_Test::testPushPop()
{
    RingFIFO<int, 5> fifo;
    CPPUNIT_ASSERT(fifo.isEmpty());
    CPPUNIT_ASSERT(!fifo.isFull());
    fifo.push(1);
    fifo.push(2);
    fifo.push(3);
    CPPUNIT_ASSERT(!fifo.isEmpty());
    CPPUNIT_ASSERT(!fifo.isFull());
    fifo.push(4);
    fifo.push(5);
    CPPUNIT_ASSERT(!fifo.isEmpty());
    CPPUNIT_ASSERT(fifo.isFull());
    fifo.push(6);// will over write 1 because it's full
    CPPUNIT_ASSERT_EQUAL(2, fifo.front());
    fifo.pop();
    CPPUNIT_ASSERT_EQUAL(3, fifo.front());
    fifo.pop();
    CPPUNIT_ASSERT_EQUAL(4, fifo.front());
    fifo.pop();
    CPPUNIT_ASSERT_EQUAL(5, fifo.front());
    fifo.pop();
    CPPUNIT_ASSERT_EQUAL(6, fifo.front());
    fifo.pop();
    CPPUNIT_ASSERT(fifo.isEmpty());
    fifo.pop();// shouldn't error out even though it's empty
    CPPUNIT_ASSERT(fifo.isEmpty());
}

int main()
{
	CppUnit::TextUi::TestRunner runner;
	runner.addTest(RingFIFO_Test::suite());
	return ! runner.run();
}
