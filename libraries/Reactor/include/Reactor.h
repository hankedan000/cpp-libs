#pragma once

#include <czmq.h>
#include "LoggingObject.h"
#include <unordered_map>
#include <vector>

template <class PROTOBUF_MSG_T>
int
protobuf_handler(
	zloop_t *loop,
	zmq_pollitem_t *item,
	void *arg);

// converts a ZMQ_EVENT_* to a string
const char *
zmq_event_str(
	uint16_t event);

// forward declaration
struct UserSocketHandlerArgs;
struct RawDataHandlerArgs;
struct MsgHandlerArgs;
struct MonitorHandlerArgs;
struct TimerHandlerArgs;

class Reactor : public LoggingObject
{
private:
	enum ZloopHandlerType
	{
		eZHT_UserSocket,
		eZHT_RawData,
		eZHT_ProtobufMsg,
		eZHT_MonitorSocket
	};

	// structure created for each registered message listener
	struct ZloopHandlerObjects
	{
		// args that are passed to the zloop_fn
		union
		{
			UserSocketHandlerArgs *usock;
			RawDataHandlerArgs *raw;
			MsgHandlerArgs *msg;
			MonitorHandlerArgs *mon;

			void *v_args;
		} args;

		// informs which underlying type is stored inside 'args'
		ZloopHandlerType handler_type;

		// function that zloop calls when event fires
		zloop_fn *handler;
	};

	// structure created for each registered timer listener
	struct TimerHandlerObjects
	{
		// args that are passed to the zloop_fn
		TimerHandlerArgs *args;
	};

public:
	/**
	 * Constructor for the Reactor
	 *
	 * @param[in] init_buffer_size
	 * The size, in bytes, to initialize of the memory buffer to.
	 * The buffer is used to receive data from all listening
	 * sockets. The user should set this size to the largest
	 * possible message that can be received across any of the
	 * listening sockets. In the event the buffer is sized too
	 * small, the reactor will dynamically allocate more memory
	 * at runtime, but this will result in message drops.
	 */
	Reactor(
		size_t init_buffer_size = 64000);

	// destructor
	~Reactor();

	/**
	 * Add a handler that simply redirect to a user callback when
	 * data is received on the socket. It's up to the user callback
	 * to perform any receive operations on the socket
	 * 
	 * @param[in] zmq_socket
	 * The zmq socket to poll on
	 * 
	 * @param[in] user_callback
	 * The method to call when data is available to read
	 * 
	 * @param[in] user_arg
	 * Argument to pass to the user callback
	 * 
	 * @return
	 * True if the listenr was successfully added, false otherwise
	 */
	bool
	add_user_socket_listener(
		void *zmq_socket,
		void (*user_callback)(void */*socket*/, void */*arg*/),
		void *user_arg);

	bool
	add_raw_data_listener(
		void *zmq_socket,
		void (*user_callback)(void */*data*/, size_t/*size*/));

	/**
	 * Adds a google::protobuf message listener to the Reactor
	 *
	 * @param[in] zmq_socket
	 * The zmq socket to poll on
	 *
	 * @param[in] user_callback
	 * The method to call when a new message is deserialized
	 *
	 * @return
	 * True if the listener was successfully added, false otherwise
	 */
	template <class PROTOBUF_MSG_T>
	bool
	add_pb_listener(
		void *zmq_socket,
		void (*user_callback)(const PROTOBUF_MSG_T &));

	/**
	 * Adds a callback to monitor for events on a ZMQ monitor socket
	 * 
	 * @param[in] monitor_socket
	 * The zmq monitor socket to read on
	 * 
	 * @param[in] user_callback
	 * Function pointer to call when events occur
	 * 
	 * @return
	 * True if the listener was successfully added, false otherwise
	 */
	bool
	add_monitor_listener(
		void *monitor_socket,
		void (*user_callback)(uint16_t /*event*/, const char */*address*/) = nullptr);

	/**
	 * Schedules a timer callback on the reactor
	 *
	 * @param[in] interval_ms
	 * The timer interval in milliseconds
	 *
	 * @param[in] times
	 * The number of timer repetitions
	 *
	 * @param[out] timer_id
	 * The scheduled timer id; -1 is returned if failures occurred
	 *
	 * @param[in] user_callback
	 * The callback to call when timer fires
	 *
	 * @param[in] user_vargs
	 * Arguments to pass to the user callback
	 *
	 * @return
	 * True if the timer was successfully scheduler, false otherwise.
	 */
	bool
	add_timer_listener(
		size_t interval_ms,
		size_t times,
		int &timer_id,
		void (*user_callback)(int /*timer_id*/, void * /*vargs*/),
		void *user_vargs);

	/**
	 * Cancel a scheduled timer
	 *
	 * @param[in] timer_id
	 * The id of the timer to cancel
	 *
	 * @return
	 * True if the timer was successfully canceled.
	 */
	bool
	cancel_timer(
		int timer_id);

	/**
	 * Starts the reactor. This method will take over the thread
	 * until the stop() or terminate() method is called.
	 *
	 * @return
	 * True if the reactor was started successfully, false
	 * otherwise.
	 */
	bool
	start();

	/**
	 * @return
	 * pointer to shared buffer used for reading from sockets
	 */
	void *
	buffer();

	/**
	 * @return
	 * the size of the shared buffer
	 */
	size_t
	buffer_size() const;

	/**
	 * @param[in] new_size
	 * the requested new size of the shared buffer
	 */
	void
	resize_buffer(
		size_t new_size);

protected:
	/**
	 * Stops all event handling (poll items, timers, etc.)
	 */
	void
	stop();

	/**
	 * Stops the reactor from handling all events and removes all
	 * registered listeners from the reactor.
	 */
	void
	cleanup();

private:
	// the zloop that this reactor owns
	zloop_t * loop_;

	// pollitems created for each listening socket
	std::vector<zmq_pollitem_t> items_;

	// handler objects created for each lisening socket
	std::vector<ZloopHandlerObjects> zloop_handler_objs_;

	// handler objects created for each scheduled timer
	std::unordered_map<int /*timer_id*/,TimerHandlerObjects> timer_handler_objs_;

	// buffer used to receive socket data
	std::vector<uint8_t> buffer_;

};

// arguments that are passed through zloop handler for raw binary data
struct UserSocketHandlerArgs
{
	/**
	 * A reference to the Reactor class. This is used for
	 * logging and for shared buffer access from within
	 * the zloop_fn handler.
	 */
	Reactor *reactor;

	/**
	 * void * argument to pass to user callback
	 */
	void *user_varg;

	/**
	 * The callback that the Reactor makes to the user code.
	 */
	void (*user_callback)(void */*socket*/, void */*arg*/);
};

// arguments that are passed through zloop handler for raw binary data
struct RawDataHandlerArgs
{
	/**
	 * A reference to the Reactor class. This is used for
	 * logging and for shared buffer access from within
	 * the zloop_fn handler.
	 */
	Reactor *reactor;

	/**
	 * The callback that the Reactor makes to the user code.
	 */
	void (*user_callback)(void */*data*/, size_t/*size*/);
};

// arguments that are passed through zloop handler
struct MsgHandlerArgs
{
	/**
	 * A reference to the Reactor class. This is used for
	 * logging and for shared buffer access from within
	 * the zloop_fn handler.
	 */
	Reactor *reactor;

	/**
	 * The callback that the Reactor makes to the user code.
	 * This is reinterpret casted to void (const MSG_T &)
	 * from within the templated event handlers.
	 */
	void *user_callback;
};

// arguments that are passed through zloop handler for ZMQ 'monitor sockets'
struct MonitorHandlerArgs
{
	/**
	 * A reference to the Reactor class. This is used for
	 * logging and for shared buffer access from within
	 * the zloop_fn handler.
	 */
	Reactor *reactor;

	/**
	 * The callback that the Reactor makes to the user code.
	 */
	void (*user_callback)(uint16_t /*event*/, const char */*address*/);
};

// arguments that are passed through zloop handler
struct TimerHandlerArgs
{
	/**
	 * A reference to the Reactor class. This is used for
	 * logging and for shared buffer access from within
	 * the zloop_fn handler.
	 */
	Reactor *reactor;

	// number of times the timer should fire
	// this is -1 if the timer runs indefinitely
	size_t times;

	// Use by the Reactor to cleanup internal data structures
	// once the timer has expired (ie. timer fired for all
	// scheduled times). If the timer has finite life (times > -1),
	// then this is initialized to 'times' and decremented each
	// time the timer fires. Once this reaches zero, the Reactor
	// will clean up its internal data structures.
	size_t ttl;

	/**
	 * The callback that the Reactor makes to the user code.
	 */
	void (*user_callback)(int /*timer_id*/, void * /*vargs*/);

	/**
	 * The void args to pass back to the user_callback
	 */
	void *user_vargs;
};

template <class PROTOBUF_MSG_T>
bool
Reactor::add_pb_listener(
	void *zmq_socket,
	void (*user_callback)(const PROTOBUF_MSG_T &))
{
	bool okay = true;

	if (zmq_socket == nullptr)
	{
		LOG4CXX_ERROR(getLogger(),
			__func__ << " - "
			"attempted to add listener with null zmq_socket");
		okay = false;
	}
	else if (user_callback == nullptr)
	{
		LOG4CXX_ERROR(getLogger(),
			__func__ << " - "
			"attempted to add listener with null user_callback");
		okay = false;
	}
	else
	{
		// create new poll item for listener
		zmq_pollitem_t new_item;
		new_item.socket = zmq_socket;
		new_item.events = ZMQ_POLLIN;

		items_.push_back(new_item);

		// create new zloop handler objects for listener
		ZloopHandlerObjects zho;
		zho.handler = protobuf_handler<PROTOBUF_MSG_T>;
		zho.handler_type = eZHT_ProtobufMsg;
		zho.args.msg = new MsgHandlerArgs();
		zho.args.msg->reactor = this;
		zho.args.msg->user_callback = (void*)user_callback;

		zloop_handler_objs_.push_back(zho);
	}

	return okay;
}

// templated zloop handler for google::protobuf messages
template <class PROTOBUF_MSG_T>
int
protobuf_handler(
	zloop_t *loop,
	zmq_pollitem_t *item,
	void *arg)
{
	MsgHandlerArgs *mha = (MsgHandlerArgs *)(arg);
	auto buffer_size = mha->reactor->buffer_size();
	int rbytes = zmq_recv(
		item->socket,
		mha->reactor->buffer(),
		buffer_size,
		ZMQ_NOBLOCK);

	if (rbytes < 0)
	{
		LOG4CXX_ERROR(mha->reactor->getLogger(),
			"zmq_recv failed with error = " << zmq_strerror(errno));
	}
	else if (rbytes > buffer_size)
	{
		LOG4CXX_ERROR(mha->reactor->getLogger(),
			"zmq_recv message was truncated!"
			" rbytes = " << rbytes << "; "
			" buffer_size = " << buffer_size << "."
			" Buffer is being grown dynamically, but concider"
			" increasing the default allocated size so this doesn't"
			" happen at runtime.");
		/**
		 * grow to twice the size necessary in hopes to prevent future
		 * resizes at runtime.
		 */
		mha->reactor->resize_buffer(rbytes * 2);
	}
	else
	{
		// deserialize datum
		static PROTOBUF_MSG_T datum;
		if (datum.ParseFromArray(mha->reactor->buffer(),rbytes))
		{
			// get protobuf handler callback
			auto pb_handler = (void (*)(const PROTOBUF_MSG_T &))mha->user_callback;

			// call the user callback
			pb_handler(datum);
		}
		else
		{
			LOG4CXX_ERROR(mha->reactor->getLogger(),
				"datum.ParseFromArray() failed!");
		}
	}

	// returning non-zero value will shutdown zloop reactor
	return 0;
}
