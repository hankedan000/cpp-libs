/**
 * File: ReactorTest.cpp
 * Author:  Daniel Hankewycz
 * Description:
 * CppUnit test for the Reactor class
 */
#include "ReactorTest.h"
#include <zmq.h>

void
on_simple_msg(
	const SimpleMessage &datum)
{
}

void
ReactorTest::setUp()
{
	reactor_ = new Reactor();
}

void
ReactorTest::tearDown()
{
	delete reactor_;
}

void
ReactorTest::null_adds()
{
	// catch null socket
	CPPUNIT_ASSERT( ! reactor_->add_pb_listener(nullptr,on_simple_msg));
	CPPUNIT_ASSERT( ! reactor_->add_pb_listener(NULL,on_simple_msg));
	CPPUNIT_ASSERT( ! reactor_->add_pb_listener(0,on_simple_msg));
}

void
ReactorTest::test()
{
	// TODO implement more tests
}

int main()
{
	CppUnit::TextUi::TestRunner runner;
	runner.addTest(ReactorTest::suite());
	runner.run();
	return 0;
}
