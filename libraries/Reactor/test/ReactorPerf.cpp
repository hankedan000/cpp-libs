#include <thread>
#include "Logging.h"
#include "Reactor.h"
#include "SimpleMessage.pb.h"
#include "tqdm.h"
#include "ZMQ_Boilerplate.h"

ZMQ_Boilerplate boilerplate("ReactorDemo");
Reactor reactor;
const unsigned int NUM_MSGS = 10000000;
const char *IPC_PERF_SOCKET = "ipc:///tmp/reactor_perf.sock";
tqdm bar;

void
on_simple_message(
	const SimpleMessage &datum)
{
	LOG4CXX_TRACE(boilerplate.getLogger(),
		"Received a SimpleMessage!\n" << datum.DebugString());
	bar.progress(datum.myint(), NUM_MSGS);
}

bool
publish_msg(
	void *pub_socket,
	const SimpleMessage &msg)
{
	bool okay = true;

	static uint8_t buffer[10000];
	msg.SerializeToArray((void*)buffer,sizeof(buffer));
	size_t samp_size = msg.ByteSizeLong();

	if (zmq_send(pub_socket,(void*)buffer,samp_size,ZMQ_DONTWAIT) < 0)
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			"zmq_send failed for message '" << msg.DebugString() << "'");
		okay = false;
	}

	return okay;
}

// child process is spawned into this method
void publisher1()
{
	void *context = zmq_ctx_new();
	void *publisher = zmq_socket(context,ZMQ_PUB);
	LOG4CXX_INFO(boilerplate.getLogger(),
		__func__ << " - " <<
		"publisher1 binding to " << IPC_PERF_SOCKET);
	int rc = zmq_bind(publisher,IPC_PERF_SOCKET);

	SimpleMessage msg;
	msg.set_mystring("Helloworld! I'm publisher1.");
	unsigned int count = 0;
	for (unsigned int i=0; i<NUM_MSGS; i++)
	{
		msg.set_myint(i);
		publish_msg(publisher,msg);
	}

	zmq_close(publisher);
	zmq_ctx_destroy(context);

	LOG4CXX_DEBUG(boilerplate.getLogger(),
		"publisher1 stopped.");
}

int main()
{
	bool okay = true;

	if ( ! Logging::was_init_from_config())
	{
		boilerplate.getLogger()->setLevel(log4cxx::Level::getInfo());
	}

	reactor.setLoggerPrefix(boilerplate.getLoggerNameFull());

	// spawn some publisher threads
	std::thread pub1(publisher1);

	void *sub1 = boilerplate.create_socket(ZMQ_SUB);
	if (okay && zmq_connect(sub1,IPC_PERF_SOCKET) < 0)
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			"Failed to connect to subscriber!");
		okay = false;
	}
	zmq_setsockopt(sub1,ZMQ_SUBSCRIBE,"",0);

	okay = okay && reactor.add_pb_listener(sub1,on_simple_message);
	if (okay)
	{
		LOG4CXX_INFO(boilerplate.getLogger(),
			"reactor is listening...");
		if ( ! reactor.start())
		{
			LOG4CXX_ERROR(boilerplate.getLogger(),
				"reactor failed to start!");
		}
		else
		{
			bar.finish();
			LOG4CXX_INFO(boilerplate.getLogger(),
				"reactor shutdown complete!");
		}
	}

	// wait for publishers to join
	pub1.join();

	return okay ? EXIT_SUCCESS : EXIT_FAILURE;
}