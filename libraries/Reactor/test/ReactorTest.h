/**
 * File: ReactorTest.h
 * Author:  Daniel Hankewycz
 * Description:
 * CppUnit test for the Reactor class
 */
#pragma once

#include "cppunit/ui/text/TestRunner.h"
#include "cppunit/TestFixture.h"
#include "cppunit/extensions/HelperMacros.h"

#include "Reactor.h"
#include "SimpleMessage.pb.h"
#include <unistd.h>
#include <vector>

class ReactorTest : public CppUnit::TestFixture
{
	CPPUNIT_TEST_SUITE(ReactorTest);
	CPPUNIT_TEST(null_adds);
	CPPUNIT_TEST(test);
	CPPUNIT_TEST_SUITE_END();

public:
	void setUp();
	void tearDown();

protected:
	void null_adds();
	void test();

private:
	Reactor *reactor_;

};
