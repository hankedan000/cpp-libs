#include "Reactor.h"

const char *
zmq_event_str(
	uint16_t event)
{
	switch (event)
	{
		case ZMQ_EVENT_CONNECTED:
			return "ZMQ_EVENT_CONNECTED";
		case ZMQ_EVENT_CONNECT_DELAYED:
			return "ZMQ_EVENT_CONNECT_DELAYED";
		case ZMQ_EVENT_CONNECT_RETRIED:
			return "ZMQ_EVENT_CONNECT_RETRIED";
		case ZMQ_EVENT_LISTENING:
			return "ZMQ_EVENT_LISTENING";
		case ZMQ_EVENT_BIND_FAILED:
			return "ZMQ_EVENT_BIND_FAILED";
		case ZMQ_EVENT_ACCEPTED:
			return "ZMQ_EVENT_ACCEPTED";
		case ZMQ_EVENT_ACCEPT_FAILED:
			return "ZMQ_EVENT_ACCEPT_FAILED";
		case ZMQ_EVENT_CLOSED:
			return "ZMQ_EVENT_CLOSED";
		case ZMQ_EVENT_CLOSE_FAILED:
			return "ZMQ_EVENT_CLOSE_FAILED";
		case ZMQ_EVENT_DISCONNECTED:
			return "ZMQ_EVENT_DISCONNECTED";
		case ZMQ_EVENT_MONITOR_STOPPED:
			return "ZMQ_EVENT_MONITOR_STOPPED";
		case ZMQ_EVENT_ALL:
			return "ZMQ_EVENT_ALL";
		case ZMQ_EVENT_HANDSHAKE_FAILED_NO_DETAIL:
			return "ZMQ_EVENT_HANDSHAKE_FAILED_NO_DETAIL";
		case ZMQ_EVENT_HANDSHAKE_SUCCEEDED:
			return "ZMQ_EVENT_HANDSHAKE_SUCCEEDED";
		case ZMQ_EVENT_HANDSHAKE_FAILED_PROTOCOL:
			return "ZMQ_EVENT_HANDSHAKE_FAILED_PROTOCOL";
		case ZMQ_EVENT_HANDSHAKE_FAILED_AUTH:
			return "ZMQ_EVENT_HANDSHAKE_FAILED_AUTH";
		default:
			printf("**unknown event** %d\n", event);
			return "**UNKNOWN**";
	}
}

int
reactor_user_socket_callback(
	zloop_t *loop,
	zmq_pollitem_t *item,
	void *arg)
{
	UserSocketHandlerArgs *usha = (UserSocketHandlerArgs *)(arg);

	// simply call user handler and let it do something with the socket
	usha->user_callback(item->socket, usha->user_varg);

	// returning non-zero value will shutdown zloop reactor
	return 0;
}

int
reactor_raw_data_callback(
	zloop_t *loop,
	zmq_pollitem_t *item,
	void *arg)
{
	RawDataHandlerArgs *rdha = (RawDataHandlerArgs *)(arg);
	auto buffer_size = rdha->reactor->buffer_size();
	int rbytes = zmq_recv(
		item->socket,
		rdha->reactor->buffer(),
		buffer_size,
		ZMQ_NOBLOCK);

	if (rbytes < 0)
	{
		LOG4CXX_ERROR(rdha->reactor->getLogger(),
			"zmq_recv failed with error = " << zmq_strerror(errno));
	}
	else if (rbytes > buffer_size)
	{
		LOG4CXX_ERROR(rdha->reactor->getLogger(),
			"zmq_recv message was truncated!"
			" rbytes = " << rbytes << "; "
			" buffer_size = " << buffer_size << "."
			" Buffer is being grown dynamically, but concider"
			" increasing the default allocated size so this doesn't"
			" happen at runtime.");
		/**
		 * grow to twice the size necessary in hopes to prevent future
		 * resizes at runtime.
		 */
		rdha->reactor->resize_buffer(rbytes * 2);
	}
	else
	{
		// perform user callback
		rdha->user_callback(rdha->reactor->buffer(), rbytes);
	}

	// returning non-zero value will shutdown zloop reactor
	return 0;
}

int
reactor_monitor_socket_callback(
	zloop_t *loop,
	zmq_pollitem_t *item,
	void *arg)
{
	MonitorHandlerArgs *mha = (MonitorHandlerArgs *)(arg);

	// First frame in message contains event number and value
	zmq_msg_t msg;
	zmq_msg_init (&msg);
	if (zmq_msg_recv (&msg, item->socket, 0) == -1)
		return 0; // Interruped, presumably
	assert (zmq_msg_more (&msg));

	uint8_t *data = (uint8_t *) zmq_msg_data (&msg);
	uint16_t event = *(uint16_t *) (data);

	// Second frame in message contains event address
	zmq_msg_init (&msg);
	if (zmq_msg_recv (&msg, item->socket, 0) == -1)
		return -1; // Interruped, presumably
	assert (!zmq_msg_more (&msg));

	char address[1024];
	data = (uint8_t *) zmq_msg_data (&msg);
	size_t size = zmq_msg_size (&msg);
	if (sizeof(address) < (size+1)) {
		LOG4CXX_ERROR(mha->reactor->getLogger(),
			__func__ << " - "
			"insufficient buffer size for address of size " << size);
		return 0;
	}
	memcpy(address, data, size);
	address[size] = 0;

	mha->user_callback(event, address);

	// returning non-zero value will shutdown zloop reactor
	return 0;
}

void
reactor_default_on_monitor_socket_event(
	uint16_t event,
	const char *address)
{
	printf("on_monitor_event() - address: %s; event: %s;\n",
		address,
		zmq_event_str(event));
}

int
reactor_timer_callback(
	zloop_t *loop,
	int timer_id,
	void *arg)
{
	TimerHandlerArgs *tha = (TimerHandlerArgs *)(arg);

	// call user handler immediately
	tha->user_callback(timer_id,tha->user_vargs);

	// handle cleanup of finite timers
	if (tha->times != -1)
	{
		if (tha->ttl-- == 0)
		{// timer fired for the last time, clean it up
			if ( ! tha->reactor->cancel_timer(timer_id))
			{
				LOG4CXX_ERROR(tha->reactor->getLogger(),
					__func__ << " - "
					"Internal Reactor error! Failed to cleanup timer after"
					" timer has reach expiration.");
			}
		}
	}

	// returning non-zero value will shutdown zloop reactor
	return 0;
}

Reactor::Reactor(
	size_t init_buffer_size)
 : LoggingObject("Reactor")
 , loop_(zloop_new())
{
	// presize buffer
	resize_buffer(init_buffer_size);
}

Reactor::~Reactor()
{
	cleanup();
}

//---------------------------------------------------------------------------
// public methods
//---------------------------------------------------------------------------

bool
Reactor::add_user_socket_listener(
	void *zmq_socket,
	void (*user_callback)(void */*socket*/, void */*arg*/),
	void *user_arg)
{
	if (zmq_socket == nullptr)
	{
		LOG4CXX_ERROR(getLogger(),
			__func__ << " - "
			"attempted to add listener with null zmq_socket");
		return false;
	}
	else if (user_callback == nullptr)
	{
		LOG4CXX_ERROR(getLogger(),
			__func__ << " - "
			"attempted to add listener with null user_callback");
		return false;
	}

	// create new poll item for listener
	zmq_pollitem_t new_item;
	new_item.socket = zmq_socket;
	new_item.events = ZMQ_POLLIN;

	items_.push_back(new_item);

	// create new message handler objects for listener
	ZloopHandlerObjects zho;
	zho.handler = reactor_user_socket_callback;
	zho.handler_type = eZHT_UserSocket;
	zho.args.usock = new UserSocketHandlerArgs();
	zho.args.usock->reactor = this;
	zho.args.usock->user_varg = user_arg;
	zho.args.usock->user_callback = user_callback;

	zloop_handler_objs_.push_back(zho);

	return true;
}

bool
Reactor::add_raw_data_listener(
	void *zmq_socket,
	void (*user_callback)(void */*data*/, size_t/*size*/))
{
	if (zmq_socket == nullptr)
	{
		LOG4CXX_ERROR(getLogger(),
			__func__ << " - "
			"attempted to add listener with null zmq_socket");
		return false;
	}
	else if (user_callback == nullptr)
	{
		LOG4CXX_ERROR(getLogger(),
			__func__ << " - "
			"attempted to add listener with null user_callback");
		return false;
	}

	// create new poll item for listener
	zmq_pollitem_t new_item;
	new_item.socket = zmq_socket;
	new_item.events = ZMQ_POLLIN;

	items_.push_back(new_item);

	// create new message handler objects for listener
	ZloopHandlerObjects zho;
	zho.handler = reactor_raw_data_callback;
	zho.handler_type = eZHT_RawData;
	zho.args.raw = new RawDataHandlerArgs();
	zho.args.raw->reactor = this;
	zho.args.raw->user_callback = user_callback;

	zloop_handler_objs_.push_back(zho);

	return true;
}

bool
Reactor::add_monitor_listener(
	void *monitor_socket,
	void (*user_callback)(uint16_t /*event*/, const char */*address*/))
{
	if (monitor_socket == nullptr)
	{
		LOG4CXX_ERROR(getLogger(),
			__func__ << " - "
			"attempted to add listener with null monitor_socket");
		return false;
	}

	// create new poll item for listener
	zmq_pollitem_t new_item;
	new_item.socket = monitor_socket;
	new_item.events = ZMQ_POLLIN;

	items_.push_back(new_item);

	// create new message handler objects for listener
	ZloopHandlerObjects zho;
	zho.handler = reactor_monitor_socket_callback;
	zho.handler_type = eZHT_MonitorSocket;
	zho.args.mon = new MonitorHandlerArgs();
	zho.args.mon->reactor = this;
	zho.args.mon->user_callback = (
		user_callback == nullptr                ?
		reactor_default_on_monitor_socket_event :
		user_callback);

	zloop_handler_objs_.push_back(zho);

	return true;
}

bool
Reactor::add_timer_listener(
	size_t interval_ms,
	size_t times,
	int &timer_id,
	void (*user_callback)(int /*timer_id*/, void * /*vargs*/),
	void *user_vargs)
{
	bool okay = true;

	if (user_callback == nullptr)
	{
		LOG4CXX_ERROR(getLogger(),
			__func__ << " - "
			"attempted to add listener with null user_callback");
		okay = false;
	}
	else
	{
		// create new message handler objects for listener
		TimerHandlerObjects tho;
		tho.args = new TimerHandlerArgs();
		tho.args->reactor = this;
		tho.args->times = times;
		tho.args->ttl = times;
		tho.args->user_callback = user_callback;
		tho.args->user_vargs = user_vargs;

		timer_id = zloop_timer(
			loop_,
			interval_ms,
			times,
			reactor_timer_callback,
			(void *)tho.args);

		if (timer_id != -1)
		{
			timer_handler_objs_[timer_id] = tho;
		}
		else
		{
			LOG4CXX_ERROR(getLogger(),
				__func__ << " - "
				"Failed to schedule timer."
				" interval_ms = " << interval_ms << ";"
				" times = " << times << ";");
			delete(tho.args);
			okay = false;
		}
	}

	return okay;
}

bool
Reactor::cancel_timer(
	int timer_id)
{
	bool okay = true;

	auto itr = timer_handler_objs_.find(timer_id);
	if (itr != timer_handler_objs_.end())
	{
		if (zloop_timer_end(loop_,timer_id) < 0)
		{
			LOG4CXX_ERROR(getLogger(),
				__func__ << " - "
				"Internal Reactor error! Failed to cancel timer " << timer_id);
			okay = false;
		}
		// free newed argument memory
		delete itr->second.args;
		timer_handler_objs_.erase(itr);
	}

	return okay;
}

bool
Reactor::start()
{
	bool okay = true;

	// make sure zloop can catch SIGINT and SIGTERM signals
	zsys_init();
	zloop_set_nonstop(loop_,false);

	for (unsigned int i=0; i<items_.size(); i++)
	{
		// add poll item to zloop
		int poller_rc = zloop_poller(
			loop_,
			&items_[i],
			zloop_handler_objs_[i].handler,
			zloop_handler_objs_[i].args.v_args);
		if (poller_rc < 0)
		{
			LOG4CXX_ERROR(getLogger(),
				__func__ << " - "
				"zloop_poller() failed for listener " << i);
			okay = false;
		}
	}

	if (okay)
	{
		LOG4CXX_DEBUG(getLogger(),
			"starting reactor");
		int start_rc = zloop_start(loop_);
		if (start_rc == 0)
		{
			LOG4CXX_DEBUG(getLogger(),
				"reactor interrupted");
		}
		else if (start_rc < 0)
		{
			LOG4CXX_ERROR(getLogger(),
				"handler canceled reactor!");
		}
		else
		{
			LOG4CXX_ERROR(getLogger(),
				"reactor received unknown return code " << start_rc);
		}
	}

	return okay;
}

void *
Reactor::buffer()
{
	return (void*)buffer_.data();
}

size_t
Reactor::buffer_size() const
{
	return buffer_.size();
}

void
Reactor::resize_buffer(
	size_t new_size)
{
	buffer_.resize(new_size);
}

//---------------------------------------------------------------------------
// protected methods
//---------------------------------------------------------------------------

void
Reactor::stop()
{
	// end all poll items
	for (unsigned int i=0; i<items_.size(); i++)
	{
		zloop_poller_end(loop_,&items_[i]);
	}
}

void
Reactor::cleanup()
{
	// stop all event handlers
	stop();

	// clean up newed handler arguments
	for (auto &zho : zloop_handler_objs_)
	{
		switch (zho.handler_type)
		{
			case eZHT_UserSocket:
				delete zho.args.usock;
				break;
			case eZHT_RawData:
				delete zho.args.raw;
				break;
			case eZHT_ProtobufMsg:
				delete zho.args.msg;
				break;
			case eZHT_MonitorSocket:
				delete zho.args.mon;
				break;
		}
	}

	for (auto &tho : timer_handler_objs_)
	{
		delete tho.second.args;
	}

	items_.clear();
	zloop_handler_objs_.clear();
	timer_handler_objs_.clear();

	// this will stop the zloop
	zsys_interrupted = 1;

	// clean up zloop
	if (loop_ != nullptr)
	{
		zloop_destroy(&loop_);
		loop_ = nullptr;
	}
}

//---------------------------------------------------------------------------
// private methods
//---------------------------------------------------------------------------
