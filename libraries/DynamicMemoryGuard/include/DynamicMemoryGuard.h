#pragma once

#include <atomic>
#include <execinfo.h>
#include <cxxabi.h>
#include <stdio.h>
#include <stdlib.h>

#define DMEM_GUARD_MAX_BACKTRACE_DEPTH 128

class DynamicMemoryGuard
{
public:
	DynamicMemoryGuard()
	{}

	static void
	thread_enable()
	{
		thread_guard_enabled_ = true;
	}

	static void
	thread_disable()
	{
		thread_guard_enabled_ = false;
	}

	static void
	process_enable()
	{
		process_guard_enabled_ = true;
	}

	static void
	process_disable()
	{
		process_guard_enabled_ = false;
	}

	static bool
	guarded()
	{
		return thread_guard_enabled_ || process_guard_enabled_;
	}

private:
	static thread_local bool thread_guard_enabled_;
	static std::atomic<bool> process_guard_enabled_;

};

/** Print a demangled stack backtrace of the caller function to FILE* out. */
void
print_stacktrace(
	FILE *out = stderr);

void*
operator new (
	size_t size);
