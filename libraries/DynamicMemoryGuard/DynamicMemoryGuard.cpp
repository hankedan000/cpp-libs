#include "DynamicMemoryGuard.h"

thread_local bool DynamicMemoryGuard::thread_guard_enabled_ = false;
std::atomic<bool> DynamicMemoryGuard::process_guard_enabled_(false);

void
print_stacktrace(
	FILE *out)
{
	// storage array for stack trace address data
	static thread_local void* addrlist[DMEM_GUARD_MAX_BACKTRACE_DEPTH+1];

	// retrieve current stack addresses
	int addrlen = backtrace(addrlist, sizeof(addrlist) / sizeof(void*));

	if (addrlen == 0)
	{
		fprintf(out, "  <empty, possibly corrupt>\n");
		return;
	}

	// resolve addresses into strings containing "filename(function+address)",
	// this array must be free()-ed
	char** symbollist = backtrace_symbols(addrlist, addrlen);

	// allocate string which will be filled with the demangled function name
	size_t funcnamesize = 256;
	char* funcname = (char*)malloc(funcnamesize);

	// iterate over the returned symbol lines. skip the first, it is the
	// address of this function.
	for (int i = 1; i < addrlen; i++)
	{
		char *begin_name = 0, *begin_offset = 0, *end_offset = 0;

		// find parentheses and +address offset surrounding the mangled name:
		// ./module(function+0x15c) [0x8048a6d]
		for (char *p = symbollist[i]; *p; ++p)
		{
			if (*p == '(')
			{
				begin_name = p;
			}
			else if (*p == '+')
			{
				begin_offset = p;
			}
			else if (*p == ')' && begin_offset)
			{
				end_offset = p;
				break;
			}
		}

		if (begin_name && begin_offset && end_offset
			&& begin_name < begin_offset)
		{
			*begin_name++ = '\0';
			*begin_offset++ = '\0';
			*end_offset = '\0';

			// mangled name is now in [begin_name, begin_offset) and caller
			// offset in [begin_offset, end_offset). now apply __cxa_demangle():

			int status;
			char* ret = abi::__cxa_demangle(begin_name,
							funcname, &funcnamesize, &status);
			if (status == 0)
			{
				// use possibly realloc()-ed string
				funcname = ret;
				fprintf(out, "  %s : %s+%s\n",
					symbollist[i], funcname, begin_offset);
			}
			else
			{
				// demangling failed. Output function name as a C function with
				// no arguments.
				fprintf(out, "  %s : %s()+%s\n",
					symbollist[i], begin_name, begin_offset);
			}
		}
		else
		{
			// couldn't parse the line? print the whole line.
			fprintf(out, "  %s\n", symbollist[i]);
		}
	}

	free(funcname);
	free(symbollist);
}

void*
operator new (
	size_t size)
{
	if (DynamicMemoryGuard::guarded())
	{
		fprintf(stderr,"---------------------------------------------------------------\n");
		fprintf(stderr,"heap allocation detected at the following call stack.\n");
		fprintf(stderr,"size = %ldbyte(s)\n",size);
		fprintf(stderr,"---------------------- backtrace start ------------------------\n");
		print_stacktrace();
		fprintf(stderr,"----------------------- backtrace end -------------------------\n");
	}
	return malloc(size);
}