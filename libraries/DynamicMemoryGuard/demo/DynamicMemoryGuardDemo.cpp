#include "DynamicMemoryGuard.h"
#include <iostream>
#include <vector>
#include <thread>

void guarded_thread()
{
	DynamicMemoryGuard::thread_enable();
	std::vector<unsigned int> v;
	v.reserve(10);
}

void unguarded_thread()
{
	std::vector<unsigned int> v;
	v.reserve(10);
}

int main()
{
	std::thread t1(guarded_thread);
	std::thread t2(unguarded_thread);

	t1.join();
	t2.join();

	return 0;
}