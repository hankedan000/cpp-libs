#pragma once

#include "cppunit/ui/text/TestRunner.h"
#include "cppunit/TestFixture.h"
#include "cppunit/extensions/HelperMacros.h"

#include "MovingAverage.h"
#include <unistd.h>

class MovingAverageTest : public CppUnit::TestFixture
{
	CPPUNIT_TEST_SUITE(MovingAverageTest);
	CPPUNIT_TEST(testAverage);
	CPPUNIT_TEST(testReset);
	CPPUNIT_TEST_SUITE_END();

public:
	void setUp();
	void tearDown();

protected:
	void testAverage();
	void testReset();

private:

};
