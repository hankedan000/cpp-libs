#include "MovingAverageTest.h"

void
MovingAverageTest::setUp()
{
}

void
MovingAverageTest::tearDown()
{
}

void
MovingAverageTest::testAverage()
{
    MovingAverage<float, 3> ma;
    ma.addValue(2.0);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(2.0, ma.getAverage(), 0.0001);
    ma.addValue(3.0);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(2.5, ma.getAverage(), 0.0001);
    ma.addValue(4.0);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(3.0, ma.getAverage(), 0.0001);
    ma.addValue(5.0);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(4.0, ma.getAverage(), 0.0001);
    ma.addValue(6.0);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(5.0, ma.getAverage(), 0.0001);
}

void
MovingAverageTest::testReset()
{
    MovingAverage<int, 2> ma;
    ma.addValue(1);
    ma.addValue(2);
    CPPUNIT_ASSERT_EQUAL(1, ma.getAverage());
    ma.reset();
    CPPUNIT_ASSERT_EQUAL(0, ma.getAverage());
}

int main()
{
	CppUnit::TextUi::TestRunner runner;
	runner.addTest(MovingAverageTest::suite());
	return ! runner.run();
}
