#pragma once

#include "RingFIFO.h"

template <typename T, int capacity>
class MovingAverage {
public:
    MovingAverage() : sum(0) {}
    void reset() {
        fifo.clear();
        sum = 0;
    }
    void addValue(const T& value) {
        if (fifo.isFull()) {
            sum -= fifo.front();
        }
        fifo.push(value);
        sum += value;
    }
    T getAverage() const {
        if (fifo.isEmpty()) {
            return sum;
        }
        return sum / fifo.size();
    }
private:
    RingFIFO<T, capacity> fifo;
    T sum;
};
