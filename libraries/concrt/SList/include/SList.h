#pragma once

namespace concrt
{
	struct SNode
	{
		void *data;
		SNode *next;
	};

	struct SList
	{
		static const int ERROR = -1;
		static const int CONERR = -2;

		SList()
		 : head_(nullptr)
		{}

		bool
		empty()
		{
			return head_ == nullptr;
		}

		SNode *
		head()
		{
			return head_;
		}

		int
		push(
			SNode *new_node)
		{
			if (new_node == nullptr)
			{
				return ERROR;
			}
			do {
				// set up `new_node->head_` so the list will still be linked 
				// correctly the instant the element is inserted
				new_node->next = head_;
			} while (!__sync_bool_compare_and_swap(&head_, new_node->next, new_node));
			return 0;
		}

		SNode *
		pop()
		{
			SNode *out_node;
			do {
				out_node = head_;
				if (out_node == nullptr)
				{
					break;
				}
			} while (!__sync_bool_compare_and_swap(&head_, out_node, out_node->next));
			return out_node;
		}

		int
		insert(
			SNode *prev_node,
			SNode *new_node)
		{
			if (prev_node == nullptr || new_node == nullptr)
			{
				return ERROR;
			}
			do {
				// set up `new_node->prev_node.next` so the list will still be linked 
				// correctly the instant the element is inserted
				new_node->next = prev_node->next;
				if ((size_t)new_node->next & 0x1)
				{
					// prev_node was removed from the list at some point
					return CONERR;
				}
			} while (!__sync_bool_compare_and_swap(&prev_node->next, new_node->next, new_node));
			return 0;
		}

		int
		remove(
			SNode *prev_node,
			SNode *curr_node)
		{
			if (prev_node == nullptr || curr_node == nullptr)
			{
				return ERROR;
			}

			// 'mark' curr_node for deletion
			size_t curr_next;
			do {
				curr_next = (size_t)curr_node->next;
				if (curr_next & 0x1)
				{
					// curr_node already marked as deleted. nothing to do
					return 0;
				}
			} while (!__sync_bool_compare_and_swap(&curr_node->next, curr_next, curr_next | 0x1));
			
			if (!__sync_bool_compare_and_swap(&prev_node->next, curr_node, (SNode *)curr_next))
			{
				// another thread has changed what prev_node points to
				// if we continue with removal retry, then we'd leave list invalid
				return CONERR;
			}
			return 0;
		}

	private:
		struct SNode *head_;
		
	};
}