#pragma once

#include "cppunit/ui/text/TestRunner.h"
#include "cppunit/TestFixture.h"
#include "cppunit/extensions/HelperMacros.h"

#include "SList.h"
#include <unistd.h>

class SListTest : public CppUnit::TestFixture
{
	CPPUNIT_TEST_SUITE(SListTest);
	CPPUNIT_TEST(testPushPop);
	CPPUNIT_TEST(testInsertDelete);
	CPPUNIT_TEST_SUITE_END();

public:
	void setUp();
	void tearDown();

protected:
	void testPushPop();
	void testInsertDelete();

private:

};
