#include <iostream>
#include <memory>
#include <thread>

#include "SList.h"

concrt::SList list;

void
test(
	unsigned int tid,
	unsigned int n_itr)
{
	const std::string TAG = std::string("tid") + std::to_string(tid) + " - ";
	const unsigned int N_NODES = 10;
	concrt::SNode my_nodes[N_NODES];
	for (unsigned int n=0; n<N_NODES; n++)
	{
		my_nodes[n].data = new unsigned int;
		*(unsigned int*)my_nodes[n].data = tid;
	}

	for (unsigned int i=0; i<n_itr; i++)
	{
		// add all my nodes to the list
		for (unsigned int n=0; n<N_NODES; n++)
		{
			list.push(&my_nodes[n]);
		}

		// make sure my nodes are on the list
		concrt::SNode *curr = list.head();
		unsigned int my_nodes_found = 0;
		while (curr != nullptr)
		{
			if (*(unsigned int*)curr->data == tid)
			{
				my_nodes_found++;
			}
			curr = curr->next;
		}
		if (my_nodes_found != N_NODES)
		{
			std::cerr << TAG << "only found " << my_nodes_found << " of my nodes on the list" << std::endl;
			return;
		}

		// find and remove all my nodes
		unsigned int nodes_removed = 0;
		while (nodes_removed != N_NODES)
		{
			concrt::SNode *prev = nullptr;
			concrt::SNode *curr = list.head();
			while (curr != nullptr)
			{
				if (*(unsigned int*)curr->data == tid)
				{
					if (prev == nullptr)
					{
						// head node is mine, so pop it
						list.pop();
						curr = list.head();
						nodes_removed++;
					}
					else
					{
						// middle node is mine, so remove it
						int rc = list.remove(prev,curr);
						if (rc == 0)
						{
							// removal was successful
							curr = curr->next;
							nodes_removed++;
						}
						else if (rc == concrt::SList::CONERR)
						{
							// another thread modified what prev points to, so restart search
							prev = nullptr;
							curr = list.head();
						}
						else
						{
							std::cerr << TAG <<
								"failed to remove node" << std::endl;
						}
					}
				}
				else
				{
					// curr is not mine, so move to next node
					prev = curr;
					curr = curr->next;
				}
			}
		}

		// make sure none of my nodes are on the list
		curr = list.head();
		my_nodes_found = 0;
		while (curr != nullptr)
		{
			if (*(unsigned int*)curr->data == tid)
			{
				my_nodes_found++;
			}
			curr = curr->next;
		}
		if (my_nodes_found != 0)
		{
			std::cerr << TAG << "found " << my_nodes_found << " of my nodes remaining on the list" << std::endl;
			return;
		}
	}
}

int main()
{
	unsigned n_threads = 4;

	std::thread threads[n_threads];

	// start all the threads
	for (unsigned int tid=0; tid<n_threads; tid++)
	{
		threads[tid] = std::thread(test,tid,1000);
	}

	// wait for all threads to join
	for (unsigned int tid=0; tid<n_threads; tid++)
	{
		threads[tid].join();
	}

	return 0;
}