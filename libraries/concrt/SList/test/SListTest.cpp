#include "SListTest.h"

void
SListTest::setUp()
{
}

void
SListTest::tearDown()
{
}

void
SListTest::testPushPop()
{
	concrt::SNode nodes[3];
	nodes[0].data = new char;
	*(char*)nodes[0].data = 'a';
	nodes[1].data = new char;
	*(char*)nodes[1].data = 'b';
	nodes[2].data = new char;
	*(char*)nodes[2].data = 'c';

	concrt::SList list;
	CPPUNIT_ASSERT(list.empty());
	CPPUNIT_ASSERT_EQUAL(0,list.push(&nodes[2]));
	CPPUNIT_ASSERT_EQUAL(0,list.push(&nodes[1]));
	CPPUNIT_ASSERT_EQUAL(0,list.push(&nodes[0]));
	CPPUNIT_ASSERT( ! list.empty());

	concrt::SNode *nut = list.head();
	CPPUNIT_ASSERT(nut != nullptr);
	CPPUNIT_ASSERT_EQUAL('a',*(char*)nut->data);
	nut = nut->next;
	CPPUNIT_ASSERT(nut != nullptr);
	CPPUNIT_ASSERT_EQUAL('b',*(char*)nut->data);
	nut = nut->next;
	CPPUNIT_ASSERT(nut != nullptr);
	CPPUNIT_ASSERT_EQUAL('c',*(char*)nut->data);

	nut = list.pop();
	CPPUNIT_ASSERT(nut != nullptr);
	CPPUNIT_ASSERT_EQUAL('a',*(char*)nut->data);
	nut = list.pop();
	CPPUNIT_ASSERT(nut != nullptr);
	CPPUNIT_ASSERT_EQUAL('b',*(char*)nut->data);
	nut = list.pop();
	CPPUNIT_ASSERT(nut != nullptr);
	CPPUNIT_ASSERT_EQUAL('c',*(char*)nut->data);

	CPPUNIT_ASSERT(list.empty());
}

void
SListTest::testInsertDelete()
{
	concrt::SNode nodes[3];
	nodes[0].data = new char;
	*(char*)nodes[0].data = 'a';
	nodes[1].data = new char;
	*(char*)nodes[1].data = 'b';
	nodes[2].data = new char;
	*(char*)nodes[2].data = 'c';

	concrt::SList list;
	CPPUNIT_ASSERT(list.empty());
	CPPUNIT_ASSERT_EQUAL(0,list.push(&nodes[0]));
	CPPUNIT_ASSERT_EQUAL(0,list.insert(&nodes[0],&nodes[1]));
	CPPUNIT_ASSERT_EQUAL(0,list.insert(&nodes[1],&nodes[2]));
	CPPUNIT_ASSERT( ! list.empty());

	concrt::SNode *nut = list.head();
	CPPUNIT_ASSERT(nut != nullptr);
	CPPUNIT_ASSERT_EQUAL('a',*(char*)nut->data);
	nut = nut->next;
	CPPUNIT_ASSERT(nut != nullptr);
	CPPUNIT_ASSERT_EQUAL('b',*(char*)nut->data);
	nut = nut->next;
	CPPUNIT_ASSERT(nut != nullptr);
	CPPUNIT_ASSERT_EQUAL('c',*(char*)nut->data);

	CPPUNIT_ASSERT_EQUAL(0,list.remove(&nodes[0],&nodes[1]));
	CPPUNIT_ASSERT_EQUAL('c',*(char*)nodes[0].next->data);
	CPPUNIT_ASSERT_EQUAL(0,list.remove(&nodes[0],&nodes[2]));
	CPPUNIT_ASSERT(nodes[0].next == nullptr);
	CPPUNIT_ASSERT(list.pop() != nullptr);

	CPPUNIT_ASSERT(list.empty());
}

int main()
{
	CppUnit::TextUi::TestRunner runner;
	runner.addTest(SListTest::suite());
	return ! runner.run();
}
