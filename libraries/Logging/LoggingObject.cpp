#include "LoggingObject.h"
#include "Int_Logging.h"

OverrideLoggingCfgObj::OverrideLoggingCfgObj(
	const std::string &filename)
{
	Int_Logging::setLoggingCfgFile(filename);
}

LoggingObject::LoggingObject(
	const std::string &loggerName)
 : LoggingObject("",loggerName)
{
}

LoggingObject::LoggingObject(
	const std::string &loggerPrefix,
	const std::string &loggerName)
 : logger_(nullptr)
 , prefix_(loggerPrefix)
 , name_(loggerName)
{
	// setup a simple configuration that logs to console
	Int_Logging::initLogging();
	setLoggerPrefix(prefix_);
}

log4cxx::LoggerPtr
LoggingObject::getLogger()
{
	return logger_;
}

std::string
LoggingObject::getLoggerPrefix()
{
	return prefix_;
}

std::string
LoggingObject::getLoggerName() const
{
	return name_;
}


std::string
LoggingObject::getLoggerNameFull() const
{
	if (prefix_ != "")
	{
		return prefix_ + "." + name_;
	}
	return name_;
}

void
LoggingObject::setLoggerPrefix(
	const std::string &loggerPrefix)
{
	prefix_ = loggerPrefix;
	logger_ = log4cxx::Logger::getLogger(getLoggerNameFull());
}
