#include "ZMQ_Appender.h"

#include <iostream>
#include <unistd.h>
#include <log4cxx/helpers/stringhelper.h>
#include <zmq.h>
#include "TheAppenderContext.h"

IMPLEMENT_LOG4CXX_OBJECT(ZMQ_Appender)

ZMQ_Appender::ZMQ_Appender()
 : buffer_(nullptr)
 , sendLocationInfo_(false)
{
	buffer_ = malloc(MAX_BUFFER_SIZE);
	sw_.enabled(false);
}

ZMQ_Appender::~ZMQ_Appender()
{
	close();
	if (buffer_ != nullptr)
	{
		free(buffer_);
		buffer_ = nullptr;
	}

	// seems to help with dropped message at shutdown
	usleep(1000);
}

void
ZMQ_Appender::append(
	const log4cxx::spi::LoggingEventPtr& event,
	log4cxx::helpers::Pool& /*p*/)
{
	bool okay = true;
	bool msgTruncated = false;

	sw_.start();

	static LogEvent datum;
	datum.set_logger(event->getLoggerName());
	if (event->getMessage().length() > MAX_MSG_LENGTH)
	{
		datum.set_msg(event->getMessage().substr(0,MAX_MSG_LENGTH));
		msgTruncated = true;
	}
	else
	{
		datum.set_msg(event->getMessage());
	}
	datum.set_level(convertLevel(event->getLevel()));
	datum.set_timestamp(event->getTimeStamp());
	datum.set_thread_name(event->getThreadName());

	if (sendLocationInfo_)
	{
		LogEvent::LocationInfo *li = datum.mutable_location_info();
		li->set_class_name(event->getLocationInformation().getClassName());
		li->set_file_name(event->getLocationInformation().getFileName());
		li->set_line_num(event->getLocationInformation().getLineNumber());
		li->set_method_name(event->getLocationInformation().getMethodName());
	}

	okay = okay && sendLogEvent(datum);

	if (okay && msgTruncated)
	{
		datum.set_level(LogEvent_Level_WARN);
		datum.set_msg(
			"Previous log message was truncated because it was longer"
			" than " + std::to_string(MAX_MSG_LENGTH) + " characters.");
		okay = okay && sendLogEvent(datum);
	}
	sw_.stop();

	if (sw_.enabled() && sw_.intervals() % 1000 == 0)
	{
		std::cout << sw_.getSummary() << std::endl;
	}
}

void
ZMQ_Appender::close()
{
	TheAppenderContext::instance()->close();
}

void
ZMQ_Appender::setOption(
	const LogString& option,
	const LogString& value)
{
	if (log4cxx::helpers::StringHelper::equalsIgnoreCase(option, LOG4CXX_STR("STOPWATCH"), LOG4CXX_STR("stopwatch")))
	{
		bool enabled;
		std::istringstream(value) >> std::boolalpha >> enabled;
		sw_.enabled(enabled);
	}
	else if (log4cxx::helpers::StringHelper::equalsIgnoreCase(option, LOG4CXX_STR("LOCATIONINFO"), LOG4CXX_STR("locationinfo")))
	{
		std::istringstream(value) >> std::boolalpha >> sendLocationInfo_;
	}
	else
	{
		AppenderSkeleton::setOption(option, value);
	}
}

bool
ZMQ_Appender::sendLogEvent(
	const LogEvent &datum)
{
	bool okay = true;

	// serialize datum into buffer
	size_t size = datum.ByteSizeLong();
	if (size > MAX_BUFFER_SIZE)
	{
		LogEvent copy(datum);
		copy.set_level(LogEvent_Level_ERROR);
		copy.set_msg(
			"Internal ZMQ_Appender ERROR! Failed to serialize LogEvent"
			" of size " + std::to_string(size) + " because it exceeds"
			" max of " + std::to_string(MAX_BUFFER_SIZE) + ". This can"
			" occur due to high serialization overhead. Log dropped.");
		sendLogEvent(copy);
		return false;
	}
	datum.SerializeToArray(buffer_,size);

	void *publisher = TheAppenderContext::instance()->get_publisher();
	int rc = zmq_send(publisher,buffer_,size,ZMQ_DONTWAIT);
	if (rc < 0)
	{
		std::cout << "ZMQ_Appender [ERROR] " <<
			"zmq_send failed for"
			" message '" << datum.msg() << "'" << std::endl;
		okay = false;
	}

	return okay;
}

LogEvent_Level
convertLevel(
	const log4cxx::LevelPtr &level)
{
	if (level->equals(log4cxx::Level::getFatal()))
	{
		return LogEvent_Level_FATAL;
	}
	else if (level->equals(log4cxx::Level::getError()))
	{
		return LogEvent_Level_ERROR;
	}
	else if (level->equals(log4cxx::Level::getWarn()))
	{
		return LogEvent_Level_WARN;
	}
	else if (level->equals(log4cxx::Level::getInfo()))
	{
		return LogEvent_Level_INFO;
	}
	else if (level->equals(log4cxx::Level::getDebug()))
	{
		return LogEvent_Level_DEBUG;
	}
	else if (level->equals(log4cxx::Level::getTrace()))
	{
		return LogEvent_Level_TRACE;
	}
	else
	{
		std::cout << "ZMQ_Appender [WARN] " <<
			__func__ << " - "
			"Converting unsupported level to WARN" << std::endl;
		return LogEvent_Level_WARN;
	}
}
