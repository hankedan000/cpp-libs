#pragma once

#include <zmq.h>

class TheAppenderContext
{
public:
	~TheAppenderContext();

	static
	TheAppenderContext *
	instance();

	void *
	get_zmq_ctx();

	void *
	get_publisher();

	void
	init();

	void
	close();

private:
	TheAppenderContext();

	// prevent copying
	TheAppenderContext(
		const TheAppenderContext &other) = delete;

private:
	// static reference to the singleton instance
	static TheAppenderContext *instance_;

	// the zmq context pointer
	void * context_;

	// the zmq publisher socket used to sending log events
	void * publisher_;

};