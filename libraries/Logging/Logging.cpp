#include "Logging.h"
#include "Int_Logging.h"
#include "TheAppenderContext.h"

namespace Logging
{
	void
	before_fork()
	{
		// shutdown zmq logging context prior to forking
		// will restore context again afterwards
		TheAppenderContext::instance()->close();
	}

	void
	after_fork()
	{
		// reconstruct zmq logging context in each forked
		// process instance
		TheAppenderContext::instance()->init();
	}

	bool
	was_init_from_config()
	{
		return Int_Logging::initialized_from_file;
	}
}