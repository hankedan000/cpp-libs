#include "TheAppenderContext.h"
#include <iostream>
#include <unistd.h>

TheAppenderContext *TheAppenderContext::instance_ = nullptr;

TheAppenderContext::TheAppenderContext()
 : context_(nullptr)
 , publisher_(nullptr)
{
	init();
}

TheAppenderContext::~TheAppenderContext()
{
	close();
	delete instance_;
	instance_ = nullptr;
}

TheAppenderContext *
TheAppenderContext::instance()
{
	if ( ! instance_)
	{
		instance_ = new TheAppenderContext();
	}
	return instance_;
}

void *
TheAppenderContext::get_zmq_ctx()
{
	return context_;
}

void *
TheAppenderContext::get_publisher()
{
	return publisher_;
}

void
TheAppenderContext::init()
{
	if ( ! context_)
	{
		context_ = zmq_ctx_new();
		publisher_ = zmq_socket(context_,ZMQ_PUB);

		const char *BIND_HOST = "ipc:///var/run/zmq_appender_log_event.sock";
		int rc = zmq_connect(publisher_,BIND_HOST);
		if (rc < 0)
		{
			std::cerr << "TheAppenderContext [ERROR] " <<
				"Failed to connect to socket @ " << BIND_HOST << std::endl;
			return;
		}

		int linger_period = 0;
		rc = zmq_setsockopt(
			publisher_,
			ZMQ_LINGER,
			&linger_period,
			sizeof(linger_period));
		if (rc < 0)
		{
			std::cerr << "TheAppenderContext [ERROR] " <<
				"Failed to set ZMQ_LINGER period on socket!" << std::endl;
			return;
		}

		// seems to help with dropped message at startup
		usleep(10000);
	}
}

void
TheAppenderContext::close()
{
	if (context_)
	{
		zmq_close(publisher_);
		publisher_ = nullptr;
		zmq_ctx_destroy(context_);
		context_ = nullptr;
	}
}
