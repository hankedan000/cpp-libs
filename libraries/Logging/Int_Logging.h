#pragma once

#include <fstream>
#include <log4cxx/basicconfigurator.h>
#include <log4cxx/propertyconfigurator.h>
#include <mutex>
#include <string>
#include <iostream>

namespace Int_Logging
{
	static std::mutex mutex;

	static bool initialized = false;
	static bool initialized_from_file = false;

	static std::string cfgPath = "/usr/local/etc/";
	static std::string cfgFilename = "logging.cfg";

	inline
	bool
	fileExists(
		const std::string &cfgFilepath)
	{
		std::ifstream ifs(cfgFilepath);
		return ifs.good();
	}

	inline
	bool
	setLoggingCfgFile(
		const std::string &newFilename)
	{
		bool okay = true;

		if ( ! initialized)
		{
			mutex.lock();
			cfgFilename = newFilename;
			mutex.unlock();
		}
		else
		{
			std::cerr << "can't set logging config file to " << newFilename <<
				" because logging has already been initialized. Try calling this"
				" function earlier in your program (ideally before any"
				" LoggingObjects are constructed)." << std::endl;
			okay = false;
		}

		return okay;
	}

	inline
	void
	initLogging()
	{
		mutex.lock();
		if ( ! initialized)
		{
			// try in global config dir first
			std::string filepath(cfgPath + cfgFilename);
			if ( ! fileExists(filepath))
			{
				// try in local dir
				filepath = cfgFilename;
			}
			
			if (fileExists(filepath))
			{
				try
				{
					// configure logging from config file
					log4cxx::PropertyConfigurator::configure(filepath);
					initialized_from_file = true;
				}
				catch (...)
				{
					// setup a simple configuration that logs to console
					log4cxx::BasicConfigurator::configure();
				}
			}
			else
			{
				// setup a simple configuration that logs to console
				log4cxx::BasicConfigurator::configure();
			}
			initialized = true;
		}
		mutex.unlock();
	}
}
