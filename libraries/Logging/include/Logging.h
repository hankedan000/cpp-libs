#pragma once

#include "LoggingObject.h"

namespace Logging
{
	/**
	 * This function must be called prior to the forking process
	 * makes the fork() system call. This method will tear down
	 * certain objects within the logging infrustruture, so
	 * no logs will be make it to the aggregator until after the
	 * fork is complete and the after_fork() method is called.
	 */
	void
	before_fork();

	/**
	 * This function must be called immediately after the forking
	 * process makes the fork() system call.
	 */
	void
	after_fork();

	/**
	 * @return
	 * True if the logging was initialized from a user defined
	 * logging config file, false otherwise.
	 */
	bool
	was_init_from_config();
}