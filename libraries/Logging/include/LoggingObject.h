#pragma once

#include <string>

// include log4cxx header files
#include <log4cxx/logger.h>

// a macro that applications can use to override the default logging
// config file name
#define SET_LOGGING_CFG_NAME(filename) \
	OverrideLoggingCfgObj __OVERRIDE_LOGGING_CFG_OBJ__(filename)

// a class used to override the default logging.cfg file
class OverrideLoggingCfgObj
{
public:
	OverrideLoggingCfgObj(
		const std::string &filename);

private:
	OverrideLoggingCfgObj() = delete;
};

class LoggingObject
{
public:
	LoggingObject(
		const std::string &loggerName);

	LoggingObject(
		const std::string &loggerPrefix,
		const std::string &loggerName);

	log4cxx::LoggerPtr
	getLogger();

	std::string
	getLoggerPrefix();

	std::string
	getLoggerName() const;

	std::string
	getLoggerNameFull() const;

	void
	setLoggerPrefix(
		const std::string &loggerPrefix);

private:
	LoggingObject() = delete;

private:
	log4cxx::LoggerPtr logger_;
	std::string prefix_;
	std::string name_;

};