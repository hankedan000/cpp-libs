#pragma once
#include "LogEvent.pb.h"

#include <log4cxx/appenderskeleton.h>
#include <log4cxx/spi/loggingevent.h>
#include <log4cxx/logger.h>
#include "Stopwatch.h"

// must include for the LOG4CXX meta object macros to work right
using namespace log4cxx;

/**
 * An appender that appends logging events to a ZeroMQ socket
 */
class ZMQ_Appender : public AppenderSkeleton
{
	public:
		DECLARE_LOG4CXX_OBJECT(ZMQ_Appender)
		BEGIN_LOG4CXX_CAST_MAP()
		LOG4CXX_CAST_ENTRY(ZMQ_Appender)
		LOG4CXX_CAST_ENTRY_CHAIN(AppenderSkeleton)
		END_LOG4CXX_CAST_MAP()

		ZMQ_Appender();

		~ZMQ_Appender();

		/**
		This method is called by the AppenderSkeleton#doAppend
		method.
		*/
		void
		append(
			const log4cxx::spi::LoggingEventPtr& event,
			log4cxx::helpers::Pool& p);

		void
		close();

		bool
		requiresLayout() const
		{
			return false;
		}

		/**
		 * Override method to get options from logging config
		 */
		void
		setOption(
			const LogString& option,
			const LogString& value);

	public:
		// maximum length of a log event's message string
		static const size_t MAX_MSG_LENGTH = 40000;

		/**
		 * maximum size of a packet sent of ZMQ socket
		 * 
		 * Note:
		 * The additional 1000bytes accomodates overhead of a serialized
		 * protobuf LogEvent message. In reality I've seen that this
		 * number is ~6bytes so this is an extreme over exproximation.
		 */
		static const size_t MAX_BUFFER_SIZE = MAX_MSG_LENGTH + 1000;

	private:
		bool
		sendLogEvent(
			const LogEvent &datum);

	private:
		Stopwatch sw_;
		void *buffer_;
		size_t bufferSize_;
		bool sendLocationInfo_;

};

LogEvent_Level
convertLevel(
	const log4cxx::LevelPtr &level);
