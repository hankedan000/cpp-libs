/**
 * File:	MultiStopwatch.cpp
 * Author:	Daniel Hankewycz
 * Description:
 * Class implementation for MultiStopwatch.
 */

#include "MultiStopwatch.h"
#include <fstream>

MultiStopwatch::MultiStopwatch()
 : enabled_(false)
{
	records_.reserve(10);
}

void
MultiStopwatch::reset()
{
	for (auto &r : records_)
	{
		r.sw->reset();
	}
}

void
MultiStopwatch::enabled(
	bool enabled)
{
	enabled_ = enabled;
}

bool
MultiStopwatch::enabled() const
{
	return enabled_;
}

int
MultiStopwatch::addRecord(
	const std::string &record_name,
	unsigned int history_size)
{
	Record new_record = {
		std::shared_ptr<Stopwatch>(new Stopwatch(true,history_size)),
		record_name
	};
	records_.push_back(new_record);
	return records_.size() - 1;
}

void
MultiStopwatch::start(
	const int &record_id)
{
	if ( ! enabled_)
	{
		return;
	}
	else if (record_id >= records_.size())
	{
		std::cerr << "invalid record_id " << record_id << std::endl;
		return;
	}
	records_[record_id].sw->start();
}

void
MultiStopwatch::stop(
	const int &record_id)
{
	if ( ! enabled_)
	{
		return;
	}
	else if (record_id >= records_.size())
	{
		std::cerr << "invalid record_id " << record_id << std::endl;
		return;
	}
	records_[record_id].sw->stop();
}

std::string
MultiStopwatch::getSummary() const
{
	std::stringstream ss;
	ss << "===================================================" << std::endl;
	ss << "MultiStopwatch Summary" << std::endl;
	ss << "===================================================" << std::endl;
	for (const auto &record : records_)
	{
		ss << "--------------------------------" << std::endl;
		ss << "record = \"" << record.name << "\"" << std::endl;
		ss << record.sw->getSummary(false) << std::endl;
		ss << "--------------------------------" << std::endl;
	}
	return ss.str();
}

bool
MultiStopwatch::saveMeasurementsToCSV(
	const std::string &filepath) const
{
	bool okay = true;

	if (Stopwatch_LIKELY( ! enabled_))
	{
		return okay;
	}

	std::ofstream ofs(filepath);
	okay = okay && ofs.good();

	if (okay)
	{
		unsigned int record_prog[records_.size()];
		for (unsigned int rr=0; rr<records_.size(); rr++)
		{
			ofs << records_[rr].name << " start(ns),";
			ofs << records_[rr].name << " duration(ns)";
			// add comma between records, except the last one
			if (rr < records_.size() - 1)
			{
				ofs << ",";
			}
			record_prog[rr] = 0;
		}
		ofs << std::endl;

		bool itemsRemain = true;
		while (itemsRemain)
		{
			// clear flag. will set back to true if there are more measurements to write
			itemsRemain = false;
			for (unsigned int rr=0; rr<records_.size(); rr++)
			{
				if (record_prog[rr] < records_[rr].sw->intervals())
				{
					ofs << std::chrono::duration_cast<std::chrono::nanoseconds>(records_[rr].sw->history_[record_prog[rr]].start.time_since_epoch()).count() << ",";
					ofs << records_[rr].sw->history_[record_prog[rr]].duration.count();
					record_prog[rr]++;
				}
				else
				{
					// no more measurements to save for this record, just put empty cells
					ofs << ",";
				}

				// add comma between records, except the last one
				if (rr < records_.size() - 1)
				{
					ofs << ",";
				}

				itemsRemain = itemsRemain || record_prog[rr] < records_[rr].sw->intervals();
			}

			ofs << std::endl;
		}
	}

	ofs.close();

	return okay;
}
