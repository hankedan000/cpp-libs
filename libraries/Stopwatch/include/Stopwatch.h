/**
 * File:	Stopwatch.h
 * Author:	Daniel Hankewycz
 * Description:
 * Class definition for a Stopwatch.
 *
 * This class allows for users to time execution of code very easily, by
 * providing a simple start/stop interface. It keeps track of different metrics
 * such as min, max, mean, etc.
 */
#ifndef STOPWATCH_H_
#define STOPWATCH_H_

#include <chrono>
#include <cmath>
#include <iostream>
#include <sstream>
#include <sys/time.h>

#define Stopwatch_LIKELY(x)   __builtin_expect(!!(x), 1) 
#define Stopwatch_UNLIKELY(x) __builtin_expect(!!(x), 0)

/**
 * Helper class used to throttle Stopwatch summary logging to
 * a specific rate/interval. This class has ~50us worth of overhead
 * so it's recommend that this method isn't called at too high of a
 * rate.
 */
class StopwatchThrottle
{
public:
   StopwatchThrottle(
      double interval_sec)
    : first_call_(true)
   {
      auto whole_sec = std::floor(interval_sec);
      throttle_interval_.tv_sec = whole_sec;
      throttle_interval_.tv_usec = (interval_sec - whole_sec) / 1.0e-6;
   }

   bool
   ready()
   {
      gettimeofday(&curr_time_, 0);

      if (first_call_)
      {
         prev_interval_start_ = curr_time_;
         first_call_ = false;
         return false;
      }

      timersub(&curr_time_,&prev_interval_start_,&diff_);
      bool ready = timercmp(&diff_,&throttle_interval_,>);
      if (ready)
      {
         prev_interval_start_ = curr_time_;
      }
      return ready;
   }

private:
   // set to false once we've throttle the first interval
   bool first_call_;

   // the previous timestamp that we crossed an interval
   timeval prev_interval_start_;

   // the duration of time the throttle will check for
   timeval throttle_interval_;

   // used to perform math and timeval comparisons
   timeval curr_time_, diff_;

};

class Stopwatch
{
public:
   struct Measurement
   {
      // the measurement's start time
      std::chrono::time_point<std::chrono::high_resolution_clock> start;
      // the measurement's duration
      std::chrono::nanoseconds duration;
   };

public:
   /**
    * Constructor for the stopwatch class
    * 
    * Allows for enabling storage of all measured event.
    * If storage is disabled, then only the min/max/mean
    * of all measured events will be stored. If storage
    * is enabled, every measured events duration will be
    * captured in a buffer and can be stored to CSV.
    * 
    * @param[in] storeEvents
    * set true to store all stopwatch events in memory
    * 
    * @param[in] historySize
    * the size of the event history buffer
    */
   Stopwatch(
      bool storeEvents = false,
      unsigned int historySize = 1024);

   /**
    * Destructor for the stopwatch class
    */
   ~Stopwatch();

   /**
    * Clears the internal state of the stopwatch.
    */
   void
   reset();

   /**
    * Enables or disables the stopwatch
    */
   void
   enabled(
      bool enabled);

   /**
    * Getter for if the stopwatch is enabled or disabled
    */
   bool
   enabled() const;

   /**
    * Call this method to begin the timing of an interval
    */
   void
   start();

   /**
    * Call this method to end the timing of an interval
    */
   void
   stop();

   /**
    * Getter the shortest interval recorded thus far
    *
    * @return
    * The duration of the interval in microseconds
    */
   std::chrono::microseconds
   min() const;

   /**
    * Getter the shortest interval recorded thus far
    *
    * @return
    * The duration of the interval in nanoseconds
    */
   std::chrono::nanoseconds
   min_ns() const;

   /**
    * Getter for the longest interval recorded thus far
    *
    * @return
    * The duration of the interval in microseconds
    */
   std::chrono::microseconds
   max() const;

   /**
    * Getter for the longest interval recorded thus far
    *
    * @return
    * The duration of the interval in nanoseconds
    */
   std::chrono::nanoseconds
   max_ns() const;

   /**
    * Getter for the average interval duration
    *
    * @return
    * The duration in microseconds
    */
   std::chrono::microseconds
   mean() const;

   /**
    * Getter for the average interval duration
    *
    * @return
    * The duration in nanoseconds
    */
   std::chrono::nanoseconds
   mean_ns() const;

   /**
    * Getter for the total time recorded across all intervals
    *
    * @return
    * The total elapsed time in microseconds
    */
   std::chrono::microseconds
   elapsed() const;

   /**
    * Getter for the total time recorded across all intervals
    *
    * @return
    * The total elapsed time in nanoseconds
    */
   std::chrono::nanoseconds
   elapsed_ns() const;

   /**
    * @return
    * The total number of intervals recorded
    */
   unsigned long long
   intervals() const;

   /**
    * @return
    * True if the stopwatch internal history buffer is full
    */
   bool
   historyFull() const;

   /**
    * Get a summary of all the Stopwatch metrics
    *
    * @param[in] oneLine
    * If true, the summary string will be all on 1 line. Default value is false
    *
    * @return
    * A string containing the summary
    */
   std::string
   getSummary(
         bool oneLine = false) const;

   bool
   saveMeasurementsToCSV(
      const std::string &filepath) const;

private:
   std::string
   engDurStr(
      const std::chrono::nanoseconds &dur_ns,
      unsigned int precision = 2) const;

private:
   bool storeEvents_;
   unsigned int historySize_;
   bool enabled_;
   bool started_;
   std::chrono::nanoseconds elapsed_;
   std::chrono::nanoseconds min_;
   std::chrono::nanoseconds max_;
   unsigned long long intervals_;

   // block memory to store measurements in
   Measurement *history_;

   // pointer into history buffer where to store the next measurement
   Measurement *historyHead_;

   // set true when storing events to memory and the history
   // buffer has reached its capacity. stopwatch measurements
   // will be dropped when this occurs.
   bool historyFull_;

   // allow MultiStopwatch to gain access to members like the history_
   friend class MultiStopwatch;

};

#endif
