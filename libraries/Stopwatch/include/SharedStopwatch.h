/**
 * File:	SharedStopwatch.h
 * Author:	Daniel Hankewycz
 * Description:
 * Class definition for a SharedStopwatch.
 *
 * This class allows for users to time execution of code very easily, by
 * providing a singleton class that can time code via a simple record based
 * interface.
 */
#pragma once

#include "Stopwatch.h"
#include <string>
#include <unordered_map>

class SharedStopwatch
{
public:
	~SharedStopwatch();

	/**
	 * Singleton instance accessor
	 *
	 * @return
	 * Pointer to the singleton instance of this class
	 */
	static
	SharedStopwatch *
	instance();

	/**
	* Clears the internal state of the stopwatch.
	*/
	void
	reset();

	/**
	 * Enables or disables the stopwatch
	 */
	void
	enabled(
		bool enabled);

	/**
	 * Getter for if the stopwatch is enabled or disabled
	 */
	bool
	enabled() const;

	/**
	 * Call this method to begin the timing of an interval
	 *
	 * @param[in] recordName
	 * A unique record name. The record will be automatically added
	 * if it doesn't already exist in the stopwatch.
	 */
	void
	start(
		const std::string &recordName);

	/**
	 * Call this method to end the timing of an interval
	 *
	 * @param[in] recordName
	 * A unique record name
	 */
	void
	stop(
		const std::string &recordName);

	/**
	 * Get a summary of all the Stopwatch metrics
	 *
	 * @return
	 * A string containing the summary
	 */
	std::string
	getSummary() const;

	/**
	 * Saves a Comma Separated Value (CSV) file report of all the stopwatch
	 * records thus far. This method will overwrite the file if it already
	 * exists.
	 *
	 * @param[in] filepath
	 * The file path to save the CSV to.
	 * examples: "summary.csv", "/tmp/results.csv", etc.
	 *
	 * @return
	 * True if successfully saves the summary, false otherwise.
	 */
	bool
	saveCSV_Summary(
		const std::string &filepath) const;

private:
	// hide default constructor
	SharedStopwatch();

private:
	static SharedStopwatch *instance_;

	// boolean used to short circuit methods calls when stopwatch is off
	bool enabled_;

	// stopwatches stored by record name
	std::unordered_map<std::string,Stopwatch> stopwatches_;

};

// convenience macro to access singleton instance
#define TheSharedStopwatch ::SharedStopwatch::instance()

/**
 * Class that starts/stops a SharedStopwatch record based on it's
 * lifetime. The record is started when the object is constructed
 * and stopped when destructed (ie. falls out of scope).
 */
class ScopedRecord
{
public:
	ScopedRecord(
		const std::string &recordName)
	 : recordName_(recordName)
	{
		TheSharedStopwatch->start(recordName_);
	}

	~ScopedRecord()
	{
		TheSharedStopwatch->stop(recordName_);
	}

private:
	std::string recordName_;

};

// convenience macro for quickly creating a ScopedRecord object
#define TIME_IT() ScopedRecord __scoped_record_obj(__PRETTY_FUNCTION__)

// convenience macro for quickly creating a ScopedRecord object
#define TIME_IT_TAG(TAG) ScopedRecord __scoped_record_obj(std::string(__PRETTY_FUNCTION__) + " # " TAG)
