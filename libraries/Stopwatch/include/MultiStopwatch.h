/**
 * File:	SharedStopwatch.h
 * Author:	Daniel Hankewycz
 * Description:
 * Class definition for a SharedStopwatch.
 *
 * This class allows for users to time execution of code very easily, by
 * providing a singleton class that can time code via a simple record based
 * interface.
 */
#pragma once

#include "Stopwatch.h"
#include <memory>
#include <string>
#include <vector>

class MultiStopwatch
{
public:
	MultiStopwatch();

	/**
	* Clears the internal state of the stopwatch.
	*/
	void
	reset();

	/**
	 * Enables or disables the stopwatch
	 */
	void
	enabled(
		bool enabled);

	/**
	 * Getter for if the stopwatch is enabled or disabled
	 */
	bool
	enabled() const;

	int
	addRecord(
		const std::string &record_name,
		unsigned int history_size = 1024);

	/**
	 * Call this method to begin the timing of an interval
	 *
	 * @param[in] record_id
	 * The record identifier to start a measurement for
	 */
	void
	start(
		const int &record_id);

	/**
	 * Call this method to end the timing of an interval
	 *
	 * @param[in] record_id
	 * The record identifier to stop a measurement for
	 */
	void
	stop(
		const int &record_id);

	/**
	 * Get a summary of all the Stopwatch metrics
	 *
	 * @return
	 * A string containing the summary
	 */
	std::string
	getSummary() const;

	/**
	 * Saves a Comma Separated Value (CSV) file report of all the stopwatch
	 * records thus far. This method will overwrite the file if it already
	 * exists.
	 *
	 * @param[in] filepath
	 * The file path to save the CSV to.
	 * examples: "summary.csv", "/tmp/results.csv", etc.
	 *
	 * @return
	 * True if successfully saves the summary, false otherwise.
	 */
	bool
	saveMeasurementsToCSV(
		const std::string &filepath) const;

private:
	// boolean used to short circuit methods calls when stopwatch is off
	bool enabled_;

	struct Record
	{
		std::shared_ptr<Stopwatch> sw;
		std::string name;
	};

	// list of records
	std::vector<Record> records_;

};
