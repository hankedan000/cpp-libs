/**
 * Filename:	StopwatchDemo.cpp
 * Author:		Daniel Hankewycz
 * Description:
 * A simple demo program for the Stopwatch class.
 */
#include "SharedStopwatch.h"

#include <iostream>
#include <vector>
#include <algorithm>
#include <ctime>
#include <cstdlib>
#include <cmath>
#include <unistd.h>

/**
 * Computes all prime factors of n
 *
 * @param[in] n
 * The integer to compute the prime factors for
 *
 * @return
 * A vector containing all the prime factors
 */
std::vector<int>
primeFactors(
      int n)
{
   TIME_IT();
   std::vector<int> factors;

   // add the number of 2's that divide n
   while (n % 2 == 0)
   {
      factors.push_back(2);
      n = n / 2;
   }

   // n must be odd at this point.  So we can skip
   // one element (Note i = i +2)
   for (int i=3; i<=std::sqrt(n); i=i+2)
   {
      // While i divides n, add i and divide n
      while (n % i == 0)
      {
         factors.push_back(i);
         n = n / i;
      }
   }

   if (n > 2)
   {
      factors.push_back(n);
   }

   return factors;
}

int main()
{
   std::cout << "========== Stopwatch Demo ==========" << "\n";
   std::srand(std::time(nullptr));// seed rand() with current time
   TheSharedStopwatch->enabled(true);

   std::vector<unsigned int> sortMe;
   const unsigned int NUM_INTS_TO_SORT = 5000;
   std::cout << "Generating " << NUM_INTS_TO_SORT << " random ints to sort\n";
   for (unsigned int i=0; i<NUM_INTS_TO_SORT; i++)
   {
      sortMe.push_back(std::rand());
   }
   {
      TIME_IT_TAG("SORTING");
      std::cout << "sorting...\n";
      std::sort(sortMe.begin(),sortMe.end());
   }

   const unsigned int NUM_INTS_TO_FACTORIZE = 10000;
   std::cout << "Prime factorizing " << NUM_INTS_TO_FACTORIZE << " integers\n";
   for (int i=1; i<=NUM_INTS_TO_FACTORIZE; i++)
   {
      auto factors = primeFactors(i);
   }

   StopwatchThrottle throttle(1.0);
   for (unsigned int i=0; i<5000; i++)
   {
      TIME_IT_TAG("THROTTLE");
      usleep(1000);
      if (throttle.ready())
      {
         std::cout << TheSharedStopwatch->getSummary() << std::endl;
      }
   }

   const std::string CSV_FILEPATH = "summary.csv";
   std::cout << "Writing CSV to \"" << CSV_FILEPATH << "\"" << std::endl;
   if ( ! TheSharedStopwatch->saveCSV_Summary(CSV_FILEPATH))
   {
      std::cerr << "Failed to save summary to " << CSV_FILEPATH << std::endl;
   }

   return 0;
}
