/**
 * Filename:	StopwatchDemo.cpp
 * Author:		Daniel Hankewycz
 * Description:
 * A simple demo program for the Stopwatch class.
 */
#include "Stopwatch.h"

#include <iostream>
#include <vector>
#include <algorithm>
#include <ctime>
#include <cstdlib>
#include <cmath>
#include <unistd.h>

void
test(
   Stopwatch &uut,
   unsigned int num_iterations)
{
   Stopwatch sw_start;
   Stopwatch sw_stop;

   for (unsigned int i=0; i<num_iterations; i++)
   {
      // measure performance of start call
      sw_start.start();
      uut.start();
      sw_start.stop();

      // measure performance of stop call
      sw_stop.start();
      uut.stop();
      sw_stop.stop();
   }

   std::cout << "Start Performance:" << std::endl;
   std::cout << sw_start.getSummary() << std::endl;
   std::cout << "Stop Performance:" << std::endl;
   std::cout << sw_stop.getSummary() << std::endl;
}

int main()
{
   std::cout << "========== Stopwatch Perf No History ==========" << "\n";   
   Stopwatch swNoHistory;
   test(swNoHistory,100000);
   std::cout << std::endl;

   std::cout << "========== Stopwatch Perf With History ==========" << "\n";   
   Stopwatch swHistory(true,100000);
   test(swHistory,100000);
   std::cout << std::endl;

   return 0;
}
