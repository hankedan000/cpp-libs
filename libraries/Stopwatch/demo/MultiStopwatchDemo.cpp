/**
 * Filename:	StopwatchDemo.cpp
 * Author:		Daniel Hankewycz
 * Description:
 * A simple demo program for the Stopwatch class.
 */
#include "MultiStopwatch.h"

#include <iostream>
#include <vector>
#include <algorithm>
#include <ctime>
#include <cstdlib>
#include <cmath>
#include <unistd.h>

MultiStopwatch msw;
struct Records
{
   static int FIRST_HALF;
   static int SECOND_HALF;
   static int SQRT_LOOP;
};
int Records::FIRST_HALF = -1;
int Records::SECOND_HALF = -1;
int Records::SQRT_LOOP = -1;

/**
 * Computes all prime factors of n
 *
 * @param[in] n
 * The integer to compute the prime factors for
 *
 * @return
 * A vector containing all the prime factors
 */
std::vector<int>
primeFactors(
      int n)
{
   std::vector<int> factors;

   // add the number of 2's that divide n
   msw.start(Records::FIRST_HALF);
   while (n % 2 == 0)
   {
      factors.push_back(2);
      n = n / 2;
   }
   msw.stop(Records::FIRST_HALF);

   // n must be odd at this point.  So we can skip
   // one element (Note i = i +2)
   msw.start(Records::SECOND_HALF);
   for (int i=3; i<=std::sqrt(n); i=i+2)
   {
      msw.start(Records::SQRT_LOOP);
      // While i divides n, add i and divide n
      while (n % i == 0)
      {
         factors.push_back(i);
         n = n / i;
      }
      msw.stop(Records::SQRT_LOOP);
   }
   msw.stop(Records::SECOND_HALF);

   if (n > 2)
   {
      factors.push_back(n);
   }

   return factors;
}

int main()
{
   std::cout << "========== MultiStopwatch Demo ==========" << "\n";

   msw.enabled(true);
   Records::FIRST_HALF = msw.addRecord("FirstHalf",10);
   Records::SECOND_HALF = msw.addRecord("SecondHalf",10);
   Records::SQRT_LOOP = msw.addRecord("SqrtLoop",1000);

   for (unsigned int i=1; i<=5; i++)
   {
      primeFactors(i * 100);
   }

   const std::string CSV_FILEPATH = "multi_stopwatch_demo.csv";
   std::cout << "Writing CSV to \"" << CSV_FILEPATH << "\"" << std::endl;
   if ( ! msw.saveMeasurementsToCSV(CSV_FILEPATH))
   {
      std::cerr << "Failed to save summary to " << CSV_FILEPATH << std::endl;
   }

   std::cout << msw.getSummary() << std::endl;

   return 0;
}
