add_executable(StopwatchDemo StopwatchDemo.cpp)
add_executable(StopwatchPerf StopwatchPerf.cpp)
add_executable(MultiStopwatchDemo MultiStopwatchDemo.cpp)

# Link the Stopwatch library to the demo
target_link_libraries(StopwatchDemo Stopwatch)
target_link_libraries(StopwatchPerf Stopwatch)
target_link_libraries(MultiStopwatchDemo Stopwatch)

# Enable C++11 features for the demo
set_target_properties(StopwatchDemo PROPERTIES CXX_STANDARD 11)
set_target_properties(StopwatchPerf PROPERTIES CXX_STANDARD 11)
set_target_properties(MultiStopwatchDemo PROPERTIES CXX_STANDARD 11)