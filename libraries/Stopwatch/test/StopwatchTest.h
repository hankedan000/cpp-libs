/**
 * File: StopwatchTest.h
 * Author:  Daniel Hankewycz
 * Description:
 * CppUnit test for the Stopwatch class
 */
#ifndef STOPWATCH_TEST_H_
#define STOPWATCH_TEST_H_

#include "cppunit/ui/text/TestRunner.h"
#include "cppunit/TestFixture.h"
#include "cppunit/extensions/HelperMacros.h"

#include "Stopwatch.h"
#include <unistd.h>

class StopwatchTest : public CppUnit::TestFixture
{
   CPPUNIT_TEST_SUITE(StopwatchTest);
   CPPUNIT_TEST(testIntervals);
   CPPUNIT_TEST(testElapsed);
   CPPUNIT_TEST(testMinMax);
   CPPUNIT_TEST(testStorage);
   CPPUNIT_TEST_SUITE_END();

public:
   void setUp();
   void tearDown();

protected:
   void testIntervals();
   void testElapsed();
   void testMinMax();
   void testStorage();

private:
   Stopwatch *sw_;

};

#endif
