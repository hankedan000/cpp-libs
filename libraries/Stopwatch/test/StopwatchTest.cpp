/**
 * File: StopwatchTest.cpp
 * Author:  Daniel Hankewycz
 * Description:
 * CppUnit test for the Stopwatch class
 */
#include "StopwatchTest.h"

void
StopwatchTest::setUp()
{
   sw_ = new Stopwatch();
}

void
StopwatchTest::tearDown()
{
   delete sw_;
}

void
StopwatchTest::testIntervals()
{
   const unsigned long long NUM_INTERVALS = 100;

   for (unsigned long long i=0; i<NUM_INTERVALS; i++)
   {
      sw_->start();
      sw_->stop();
   }

   CPPUNIT_ASSERT_EQUAL(NUM_INTERVALS,sw_->intervals());
}

void
StopwatchTest::testElapsed()
{
   const long SLEEP_DUR = 20000;
   const long NUM_ITERATIONS = 5;

   CPPUNIT_ASSERT_EQUAL(0,(int)sw_->elapsed().count());

   for (long i=0; i<NUM_ITERATIONS; i++)
   {
      sw_->start();
      usleep(SLEEP_DUR);
      sw_->stop();
   }

   const double actual_elapsed = sw_->elapsed().count();
   const double expect_elapsed = NUM_ITERATIONS * SLEEP_DUR;
   CPPUNIT_ASSERT_DOUBLES_EQUAL(expect_elapsed,actual_elapsed,(double)SLEEP_DUR/2);
}

void
StopwatchTest::testMinMax()
{
   const long SLEEP_STEP = 500;
   const long NUM_ITERATIONS = 10;

   CPPUNIT_ASSERT_EQUAL(0,(int)sw_->elapsed().count());

   for (long i=1; i<=NUM_ITERATIONS; i++)
   {
      sw_->start();
      usleep(SLEEP_STEP * i);
      sw_->stop();
   }

   CPPUNIT_ASSERT(sw_->min().count() > 0);
   CPPUNIT_ASSERT(sw_->min().count() < (SLEEP_STEP * 2));

   CPPUNIT_ASSERT(sw_->max().count() > (SLEEP_STEP * (NUM_ITERATIONS - 1)));
   CPPUNIT_ASSERT(sw_->max().count() < (SLEEP_STEP * (NUM_ITERATIONS + 1)));
}

void
StopwatchTest::testStorage()
{
   const long SLEEP_STEP = 500;
   const long NUM_ITERATIONS = 10;

   Stopwatch swMem(true,NUM_ITERATIONS);

   // fill history until 1 entry is remaining
   for (long i=1; i<=(NUM_ITERATIONS-1); i++)
   {
      swMem.start();
      usleep(SLEEP_STEP * i);
      swMem.stop();
      CPPUNIT_ASSERT_EQUAL(i,(long)swMem.intervals());
      CPPUNIT_ASSERT_EQUAL(false,swMem.historyFull());
   }

   // add 1 more entry and make sure history is full now
   swMem.start();
   usleep(SLEEP_STEP);
   swMem.stop();
   CPPUNIT_ASSERT_EQUAL(NUM_ITERATIONS,(long)swMem.intervals());
   CPPUNIT_ASSERT_EQUAL(true,swMem.historyFull());
}

int main()
{
   CppUnit::TextUi::TestRunner runner;
   runner.addTest(StopwatchTest::suite());
   return ! runner.run();
}
