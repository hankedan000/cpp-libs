/**
 * File:	Stopwatch.cpp
 * Author:	Daniel Hankewycz
 * Description:
 * Stopwatch class implementation
 */
#include "Stopwatch.h"

#include <fstream>
#include <iomanip>

Stopwatch::Stopwatch(
   bool storeEvents,
   unsigned int historySize)
 : storeEvents_(storeEvents)
 , historySize_(0)
 , enabled_(true)
 , history_(nullptr)
 , historyFull_(false)
{
   historySize_ = (storeEvents ? historySize : 1);
   history_ = new Measurement[historySize_];
   reset();
}

Stopwatch::~Stopwatch()
{
   if (history_)
   {
      delete history_;
      history_ = nullptr;
   }
}

void
Stopwatch::reset()
{
   started_ = false;
   elapsed_ = std::chrono::nanoseconds::zero();
   min_ = std::chrono::nanoseconds::max();
   max_ = std::chrono::nanoseconds::min();
   intervals_ = 0;
   historyHead_ = history_;
   historyFull_ = false;
}

void
Stopwatch::enabled(
      bool enabled)
{
   if (started_ && ! enabled)
   {
      // if we were in the middle of an interval, then stop the interval
      stop();
   }
   enabled_ = enabled;
}

bool
Stopwatch::enabled() const
{
   return enabled_;
}

void
Stopwatch::start()
{
   if (Stopwatch_LIKELY(! enabled_))
   {
      return;
   }
   else if (Stopwatch_UNLIKELY(storeEvents_ && historyFull_))
   {
      return;
   }
   started_ = true;
   historyHead_->start = std::chrono::high_resolution_clock::now();
}

void
Stopwatch::stop()
{
   if (Stopwatch_LIKELY(! enabled_))
   {
      return;
   }
   else if (Stopwatch_UNLIKELY(storeEvents_ && historyFull_))
   {
      return;
   }
   auto stop = std::chrono::high_resolution_clock::now();
   if (Stopwatch_LIKELY(started_))
   {
      historyHead_->duration = std::chrono::duration_cast<std::chrono::nanoseconds>(
         stop - historyHead_->start);

      min_ = std::min(min_,historyHead_->duration);
      max_ = std::max(max_,historyHead_->duration);
      elapsed_ += historyHead_->duration;
      intervals_++;
      historyFull_ = intervals_ >= historySize_;

      if (storeEvents_ && Stopwatch_UNLIKELY(! historyFull_))
      {
         historyHead_++;
      }
   }
   started_ = false;
}

std::chrono::microseconds
Stopwatch::min() const
{
   return std::chrono::duration_cast<std::chrono::microseconds>(min_ns());
}

std::chrono::nanoseconds
Stopwatch::min_ns() const
{
   return min_;
}

std::chrono::microseconds
Stopwatch::max() const
{
   return std::chrono::duration_cast<std::chrono::microseconds>(max_ns());
}

std::chrono::nanoseconds
Stopwatch::max_ns() const
{
   return max_;
}

std::chrono::microseconds
Stopwatch::mean() const
{
   return std::chrono::duration_cast<std::chrono::microseconds>(mean_ns());
}

std::chrono::nanoseconds
Stopwatch::mean_ns() const
{
   if (intervals_ > 0)
   {
      return elapsed_ / intervals_;
   }
   else
   {
      return std::chrono::nanoseconds::zero();
   }
}

std::chrono::microseconds
Stopwatch::elapsed() const
{
   return std::chrono::duration_cast<std::chrono::microseconds>(elapsed_ns());
}

std::chrono::nanoseconds
Stopwatch::elapsed_ns() const
{
   return elapsed_;
}

unsigned long long
Stopwatch::intervals() const
{
   return intervals_;
}

bool
Stopwatch::historyFull() const
{
   return historyFull_;
}

std::string
Stopwatch::getSummary(
      bool oneLine) const
{
   std::stringstream ss;
   char ending = (oneLine ? ' ' : '\n');

   ss << "intervals = " << intervals()          << ending;
   ss << "min = "       << engDurStr(min_ns())  << ending;
   ss << "max = "       << engDurStr(max_ns())  << ending;
   ss << "mean = "      << engDurStr(mean_ns()) << ending;
   ss << "elapsed = "   << engDurStr(elapsed_ns());

   return ss.str();
}

bool
Stopwatch::saveMeasurementsToCSV(
   const std::string &filepath) const
{
   bool okay = true;

   if (Stopwatch_LIKELY( ! enabled_))
   {
      return okay;
   }
   else if ( ! storeEvents_)
   {
      return false;
   }

   std::ofstream ofs(filepath);
   okay = okay && ofs.good();

   if (okay)
   {
      ofs << "start(ns),duration(ns)" << std::endl;
      for (unsigned int i=0; i<intervals(); i++)
      {
         ofs << std::chrono::duration_cast<std::chrono::nanoseconds>(history_[i].start.time_since_epoch()).count() << ",";
         ofs << history_[i].duration.count() << std::endl;
      }
   }

   ofs.close();

   return okay;
}

std::string
Stopwatch::engDurStr(
   const std::chrono::nanoseconds &dur_ns,
   unsigned int precision) const
{
   std::stringstream ss;
   const double NS_IN_US = 1000;
   const double NS_IN_MS = NS_IN_US * 1000;
   const double NS_IN_S = NS_IN_MS * 1000;

   ss << std::fixed << std::setprecision(precision);

   if (dur_ns.count() < NS_IN_US)
   {
      ss << dur_ns.count() << "ns";
   }
   else if (dur_ns.count() < NS_IN_MS)
   {
      ss << (dur_ns.count() / NS_IN_US) << "us";
   }
   else if (dur_ns.count() < NS_IN_S)
   {
      ss << (dur_ns.count() / NS_IN_MS) << "ms";
   }
   else
   {
      ss << (dur_ns.count() / NS_IN_S) << "s";
   }

   return ss.str();
}
