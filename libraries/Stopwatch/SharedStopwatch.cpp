/**
 * File:	SharedStopwatch.cpp
 * Author:	Daniel Hankewycz
 * Description:
 * Class implementation for SharedStopwatch.
 */

#include "SharedStopwatch.h"
#include <fstream>

// initialize static singleton instance member
SharedStopwatch *SharedStopwatch::instance_ = nullptr;


SharedStopwatch::~SharedStopwatch()
{
	if (instance_)
	{
		delete instance_;
		instance_ = nullptr;
	}
}

SharedStopwatch *
SharedStopwatch::instance()
{
	if (Stopwatch_UNLIKELY(instance_ == nullptr))
	{
		instance_ = new SharedStopwatch();
	}
	return instance_;
}

void
SharedStopwatch::reset()
{
	if (Stopwatch_LIKELY( ! enabled_))
	{
		return;
	}

	for (auto &sw : stopwatches_)
	{
		sw.second.reset();
	}
}

void
SharedStopwatch::enabled(
	bool enabled)
{
	enabled_ = enabled;
}

bool
SharedStopwatch::enabled() const
{
	return enabled_;
}

void
SharedStopwatch::start(
	const std::string &recordName)
{
	if (Stopwatch_LIKELY( ! enabled_))
	{
		return;
	}

	stopwatches_[recordName].start();
}

void
SharedStopwatch::stop(
	const std::string &recordName)
{
	if (Stopwatch_LIKELY( ! enabled_))
	{
		return;
	}

	stopwatches_[recordName].stop();
}

std::string
SharedStopwatch::getSummary() const
{
	if (Stopwatch_LIKELY( ! enabled_))
	{
		return "";
	}

	std::stringstream ss;
	ss << "===================================================" << std::endl;
	ss << "SharedStopwatch Summary" << std::endl;
	ss << "===================================================" << std::endl;
	for (const auto &sw : stopwatches_)
	{
		ss << "--------------------------------" << std::endl;
		ss << "record = \"" << sw.first << "\"" << std::endl;
		ss << sw.second.getSummary(false) << std::endl;
		ss << "--------------------------------" << std::endl;
	}

	return ss.str();
}

bool
SharedStopwatch::saveCSV_Summary(
	const std::string &filepath) const
{
	bool okay = true;

	if (Stopwatch_LIKELY( ! enabled_))
	{
		return okay;
	}

	std::ofstream ofs(filepath);
	okay = okay && ofs.good();

	if (okay)
	{
		ofs << "Record Name,Intervals,Elapsed(us),Min(us),Mean(us),Max(us)" << std::endl;
		for (const auto &sw : stopwatches_)
		{
			ofs << "\"" << sw.first            << "\",";// record name
			ofs << sw.second.intervals()       << ",";
			ofs << sw.second.elapsed().count() << ",";
			ofs << sw.second.min().count()     << ",";
			ofs << sw.second.mean().count()    << ",";
			ofs << sw.second.max().count()     << std::endl;
		}
	}

	ofs.close();

	return okay;
}

SharedStopwatch::SharedStopwatch()
 : enabled_(false)
{}
