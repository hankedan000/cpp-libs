#include "BitfieldUtils.h"
#include <iostream>
#include <iomanip>

static const Utils::BitfieldDescriptor MyBitfield = {
	"My cool control word",// bitfield name
	{// bit fields
		{"reset", "resets the processor", BU_START_BIT_WIDTH(0, 1)},
		{"busy", "processor busy", BU_START_BIT_WIDTH(1, 1)},
		{"reserve", "", BU_START_BIT_WIDTH(2, 2)},
		{"msg_ctrl_state", "current state of message controller", BU_START_STOP_BITS(4,7)},
		{"dma_length", "length of the DMA transaction in words", BU_START_BIT_WIDTH(16, 16)}
	}
};

static const Utils::BitfieldDescriptor BFD_SPI_Control = {
	"SPI Control Register",// bitfield name
	{// bit fields
		{"loopback",            "", 0, 1},
		{"SPI system enable",   "", 1, 1},
		{"Master",              "", 2, 1},
		{"Clock Polarity",      "", 3, 1},
		{"Clock Phase",         "", 4, 1},
		{"Tx FIFO Reset",       "", 5, 1},
		{"Rx FIFO Reset",       "", 6, 1},
		{"Master SS assertion", "", 7, 1}
	}
};

static const Utils::BitfieldDescriptor BFD_SPI_Status = {
	"SPI Status Register",// bitfield name
	{// bit fields
		{"Rx Empty",         "", 0,  1, Utils::Color::Normal},
		{"Rx Full",          "", 1,  1, Utils::Color::Red},
		{"Tx Empty",         "", 2,  1, Utils::Color::Normal},
		{"Tx Full",          "", 3,  1, Utils::Color::Red},
		{"Mode Fault",       "", 4,  1, Utils::Color::Red},
		{"Slave Mode",       "", 5,  1, Utils::Color::Normal},
		{"CPOL CPHA Error",  "", 6,  1, Utils::Color::Red},
		{"Slave Mode Error", "", 7,  1, Utils::Color::Red},
		{"MSB Error",        "", 8,  1, Utils::Color::Red},
		{"Loopback Error",   "", 9,  1, Utils::Color::Red},
		{"Command Error",    "", 10, 1, Utils::Color::Red}
	}
};

static const Utils::BitfieldDescriptor BFD_SPI_GlobalIntrEn = {
	"SPI Global Interrupt Enable Register",// bitfield name
	{// bit fields
		{"Global Interrupt Enable", "", 31, 1}
	}
};

static const Utils::BitfieldDescriptor BFD_SPI_IntrStatus = {
	"SPI Interrupt Status Register",// bitfield name
	{// bit fields
		{"Mode Fault Error",       "", 0,  1, Utils::Color::Red},
		{"Slave Mode Fault Error", "", 1,  1, Utils::Color::Red},
		{"DTR Empty",              "", 2,  1, Utils::Color::Normal},
		{"DTR Underrun",           "", 3,  1, Utils::Color::Red},
		{"DRR Full",               "", 4,  1, Utils::Color::Red},
		{"DRR Overrun",            "", 5,  1, Utils::Color::Red},
		{"Tx FIFO Half Empty",     "", 6,  1, Utils::Color::Normal},
		{"Slave Select Mode",      "", 7,  1, Utils::Color::Normal},
		{"DRR Not Empty",          "", 8,  1, Utils::Color::Red},
		{"CPOL CPHA Error",        "", 9,  1, Utils::Color::Red},
		{"Slave Mode Error",       "", 10, 1, Utils::Color::Red},
		{"MSB Error",              "", 11, 1, Utils::Color::Red},
		{"Loopback Error",         "", 12, 1, Utils::Color::Red},
		{"Command Error",          "", 13, 1, Utils::Color::Red}
	}
};

static const Utils::BitfieldDescriptor BFD_SPI_IntrEn = {
	"SPI Interrupt Enable Register",// bitfield name
	{// bit fields
		{"Mode Fault Error",       "", 0,  1},
		{"Slave Mode Fault Error", "", 1,  1},
		{"DTR Empty",              "", 2,  1},
		{"DTR Underrun",           "", 3,  1},
		{"DRR Full",               "", 4,  1},
		{"DRR Overrun",            "", 5,  1},
		{"Tx FIFO Half Emtpy",     "", 6,  1},
		{"Slave Select Mode",      "", 7,  1},
		{"DRR Not Empty",          "", 8,  1},
		{"CPOL CPHA Error",        "", 9,  1},
		{"Slave Mode Error",       "", 10, 1},
		{"MSB Error",              "", 11, 1},
		{"Loopback Error",         "", 12, 1},
		{"Command Error",          "", 13, 1}
	}
};

int main()
{
	std::cout << Utils::getBitfieldSummaryString(0x40e0051,MyBitfield) << std::endl;
	std::cout << Utils::getBitfieldSummaryString(0xff,BFD_SPI_Control) << std::endl;
	std::cout << Utils::getBitfieldSummaryString(0xff,BFD_SPI_Status) << std::endl;
	std::cout << Utils::getBitfieldSummaryString(0xff,BFD_SPI_GlobalIntrEn) << std::endl;
	std::cout << Utils::getBitfieldSummaryString(0xff,BFD_SPI_IntrStatus) << std::endl;
	return 0;
}