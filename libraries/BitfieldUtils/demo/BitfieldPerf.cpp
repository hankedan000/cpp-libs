#include "BitfieldUtils.h"
#include <iostream>
#include <iomanip>
#include "tqdm.h"

static const Utils::BitfieldDescriptor MyBitfield = {
	"My cool control word",// bitfield name
	{// bit fields
		{"reset", "resets the processor", 0, 1},
		{"busy", "processor busy", 1, 1},
		{"reserve", "", 2, 2},
		{"msg_ctrl_state", "current state of message controller", 4, 4},
		{"dma_length", "length of the DMA transaction in words", 16, 16}
	}
};

int main()
{
	tqdm bar;
	const unsigned int TOTAL = 1000000;
	for (unsigned int i=0; i < TOTAL; i++)
	{
		std::string str = Utils::getBitfieldSummaryString(0x40e0051,MyBitfield);
		bar.progress(i,TOTAL);
	}
	bar.finish();

	return 0;
}