add_library(
	BitfieldUtils
		BitfieldUtils.cpp
)
target_link_libraries(
	BitfieldUtils
)
target_include_directories(BitfieldUtils PUBLIC "include")
file(GLOB_RECURSE BitfielUtils_INCLUDE_FILES "include/*.h")
set_target_properties(
	BitfieldUtils PROPERTIES
		PUBLIC_HEADER "${BitfielUtils_INCLUDE_FILES}"
)
install(
	TARGETS BitfieldUtils
	ARCHIVE
		DESTINATION lib
	LIBRARY
		DESTINATION lib
	PUBLIC_HEADER
		DESTINATION include
)

add_subdirectory(demo)
