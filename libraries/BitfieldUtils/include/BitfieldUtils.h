#pragma once

#include <array>
#include <cmath>
#include <stdint.h>
#include <string>
#include <sstream>
#include <vector>

#define BU_START_BIT_WIDTH(START_BIT, WIDTH) (START_BIT), (WIDTH)
#define BU_START_STOP_BITS(START_BIT, STOP_BIT) (START_BIT), (STOP_BIT - START_BIT + 1)

namespace Utils
{
	enum Color
	{
		Normal = 0,
		Red = 1,
		Yellow = 2
	};

	static const std::array<std::string,3> COLOR_TABLE = {
		"\x1B[37m",// white
		"\x1B[31m",// red
		"\x1B[33m" // yellow
	};

	struct FieldDescriptor
	{
		// short name for the field
		std::string fieldName;

		// 1 line description of field
		std::string fieldDescription;

		// field's offset from the LSB
		uint8_t fieldBitOffset;

		// size of the field in bits (don't care for)
		uint8_t fieldBitWidth;

		// size of the field in bits (don't care for)
		uint8_t colorIfSet = Color::Normal;
	};

	struct BitfieldDescriptor
	{
		std::string bitfieldName;
		std::vector<FieldDescriptor> fieldDescriptors;
	};

	template<typename VALUE>
	std::string
	getBitfieldSummaryString(
		const VALUE &v,
		const BitfieldDescriptor &bitFieldDescriptor)
	{
		std::stringstream ss;
		const unsigned int VALUE_FULL_WIDTH = sizeof(VALUE) * 8;
		static char buffer[100];
		const unsigned int fdSize = bitFieldDescriptor.fieldDescriptors.size();

		int highestBitToPrint = 0;
		for (unsigned int i=0; i<fdSize; i++)
		{
			auto &fd(bitFieldDescriptor.fieldDescriptors[i]);
			highestBitToPrint = std::max(highestBitToPrint, fd.fieldBitOffset + fd.fieldBitWidth - 1);
		}
		highestBitToPrint = std::ceil(highestBitToPrint / 4.0) * 4 - 1;

		ss << bitFieldDescriptor.bitfieldName << ": 0x" << std::hex << v << std::endl;
		for (unsigned int i=0; i<fdSize; i++)
		{
			auto &fd(bitFieldDescriptor.fieldDescriptors[i]);
			const uint64_t WIDTH_MASK = (1U << fd.fieldBitWidth) - 1;
			const VALUE inPlaceMask = WIDTH_MASK << fd.fieldBitOffset;
			const VALUE maskedValue = v & inPlaceMask;

			uint64_t singleBitMask = 1U << highestBitToPrint;
			unsigned int lsb = fd.fieldBitOffset;
			unsigned int msb = lsb + fd.fieldBitWidth - 1;
			unsigned int bIdx = 0;
			for (int b=highestBitToPrint; b>=0; b--)
			{
				if (b >= lsb && b <= msb)
				{
					buffer[bIdx++] = ((maskedValue & singleBitMask) ? '1' : '0');
				}
				else
				{
					buffer[bIdx++] = '.';
				}
				
				if ((b & 0x3) == 0)
				{
					buffer[bIdx++] = ' ';
				}

				singleBitMask >>= 1;
			}
			buffer[bIdx++] = '\0';
			ss << buffer;

			auto fieldVal = (maskedValue >> fd.fieldBitOffset);
			const std::string &color(COLOR_TABLE[fieldVal ? fd.colorIfSet : Color::Normal]);
			if (fd.fieldBitWidth == 1)
			{// single bit value
				ss << color << fd.fieldName << "\033[0m: " << fieldVal << std::endl;
			}
			else
			{// word field display hex and dec values
				ss << color << fd.fieldName << "\033[0m: " << fieldVal << "(0x" << std::hex << fieldVal << ")" << std::endl;
			}
		}

		return ss.str();
	}
}