#!/bin/bash

java_bin=java
REQUIRED_JAVA_VERSION="1.8"

test_java_version() {
	java_bin=$1
	version_str=$2
	if $java_bin -version 2>&1 | grep version | grep $version_str; then
		# version matches
		# echo "$java_bin: Version matches!"
		return 0
	else
		# version doesn't match
		# echo "$java_bin: Version doesn't match"
		return -1
	fi
}

get_java_options() {
	find /usr/lib/ -path \*/bin/java
}

if ! test_java_version $java_bin $REQUIRED_JAVA_VERSION; then
	java_bin=""

	# search for alternatives
	java_options=$(get_java_options)
	for java_option in $java_options; do
		if test_java_version $java_option $REQUIRED_JAVA_VERSION; then
			java_bin=$java_option
			break
		fi
	done
fi

if [[ "$java_bin" == "" ]]; then
	echo "Couldn't find java version $REQUIRED_JAVA_VERSION on the system!"
	return -1
else
	eval "nohup $java_bin -jar /opt/apache-chainsaw-2.0.0/apache-chainsaw-2.0.0.jar > /tmp/chainsaw.log &"
fi
