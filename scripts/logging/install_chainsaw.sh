#!/bin/bash

wget http://mirrors.advancedhosters.com/apache/logging/chainsaw/2.0.0/apache-chainsaw-2.0.0-bin.tar.gz
tar -xzf apache-chainsaw-2.0.0-bin.tar.gz

echo "Installing chainsaw into /opt/apache-chainsaw-2.0.0 requires root"
if [ "$EUID" != 0 ]; then
	sudo mv apache-chainsaw-2.0.0 /opt/
else
	mv apache-chainsaw-2.0.0 /opt/
fi

rm apache-chainsaw-2.0.0-bin.tar.gz*