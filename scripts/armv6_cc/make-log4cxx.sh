#!/bin/bash
# create a directory to install to
INSTALL_PATH=$(pwd)/local
mkdir -p $INSTALL_PATH

# wget https://archive.apache.org/dist/logging/log4cxx/0.10.0/apache-log4cxx-0.10.0.tar.gz
# tar -xzf apache-log4cxx-0.10.0.tar.gz

# build and install libzmq compiler locally
pushd apache-log4cxx-0.10.0
	./configure --host=arm-linux-gnueabihf --prefix=$INSTALL_PATH
	make -j$(nproc)
	make install
popd

# clean up source tree
# rm -rf apache-log4cxx*
