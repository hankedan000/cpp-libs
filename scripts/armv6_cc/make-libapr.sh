#!/bin/bash
# create a directory to install to
INSTALL_PATH=$(pwd)/local
mkdir -p $INSTALL_PATH

# wget https://archive.apache.org/dist/apr/apr-1.4.8.tar.gz
# tar -xzf apr-1.4.8.tar.gz

# build and install libzmq compiler locally
pushd apr-1.4.8
	./configure \
		--build=x86_64 \
		--host=arm-linux-gnueabihf \
		--prefix=$INSTALL_PATH \
		ac_cv_file__dev_zero=yes \
		ac_cv_func_setpgrp_void=yes \
		apr_cv_tcp_nodelay_with_cork=yes
	make -j$(nproc)
	make install
popd

# clean up source tree
# rm -rf apache-log4cxx*
