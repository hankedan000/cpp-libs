#!/bin/bash
CPP_LIB_PATH=$(pwd)/local/bin
mkdir -p $CPP_LIB_PATH
echo "Adding cpp-libs to PATH."
export PATH=$PATH:$CPP_LIB_PATH
