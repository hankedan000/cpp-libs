#!/bin/bash
# create a directory to install to
INSTALL_PATH=$(pwd)/local
mkdir -p $INSTALL_PATH

# clone protobuf source code
TAG="v3.12.4"
URL="https://github.com/protocolbuffers/protobuf"
git clone --depth 1 --branch $TAG $URL

# build and install protobuf compiler locally
pushd protobuf
	./autogen.sh
	./configure --prefix=$INSTALL_PATH
	make -j$(nproc)
	make install
popd

# clean up source tree
rm -rf protobuf

# source environment so we can use 'protoc'
source env.sh
