#!/bin/bash
# create a directory to install to
INSTALL_PATH=$(pwd)/local
mkdir -p $INSTALL_PATH

git clone git://anongit.freedesktop.org/git/libreoffice/cppunit/
cd cppunit
./autogen.sh
./configure --prefix=$INSTALL_PATH
make
make install
cd ..
rm -rf cppunit
