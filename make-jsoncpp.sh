#!/bin/bash
# create a directory to install to
INSTALL_PATH=$(pwd)/local
mkdir -p $INSTALL_PATH

# clone libzmq source code
TAG="1.9.3"
URL="https://github.com/open-source-parsers/jsoncpp.git"
git clone --depth 1 --branch $TAG $URL

# build and install jsoncpp locally
mkdir -p jsoncpp/build
pushd jsoncpp/build
	cmake -DCMAKE_INSTALL_PREFIX=$INSTALL_PATH ..
	make -j$(nproc)
	make install
popd

# clean up source tree
rm -rf jsoncpp
