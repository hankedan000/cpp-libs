#!/bin/bash
# create a directory to install to
INSTALL_PATH=$(pwd)/local
mkdir -p $INSTALL_PATH

# clone libzmq source code
TAG="add-subproject-support"
URL="https://github.com/hankedan000/argparse.git"
git clone --depth 1 --branch $TAG $URL

# build and install libzmq compiler locally
mkdir -p argparse/build
pushd argparse/build
	cmake -DCMAKE_INSTALL_PREFIX=$INSTALL_PATH ..
	make -j$(nproc)
	make install
popd

# clean up source tree
#rm -rf argparse
