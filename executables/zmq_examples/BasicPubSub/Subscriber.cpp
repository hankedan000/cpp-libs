// Basic Subscriber
#include <zmq.h>
#include <iostream>
#include <unistd.h>

#include "ZMQ_Boilerplate.h"

SET_LOGGING_CFG_NAME("logging.cfg");
ZMQ_Boilerplate boilerplate("Subscriber");

int main()
{
	bool okay = true;
	// Socket to listen to publisher
	void *subscriber = boilerplate.create_socket(ZMQ_SUB);
	int rc = zmq_connect(subscriber,"tcp://localhost:5556");
	rc = zmq_setsockopt(subscriber,ZMQ_SUBSCRIBE,"",0);
	if (rc < 0)
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			"Failed to connect to publisher!");
		okay = false;
	}
	else
	{
		LOG4CXX_INFO(boilerplate.getLogger(),
			"Listening to publisher...");
		for (unsigned int i=0; okay && i<10; i++)
		{
			uint32_t count = 0;
			zmq_recv(subscriber,&count,sizeof(count),0);
			LOG4CXX_INFO(boilerplate.getLogger(),
				"Received " << count << "!");
		}
	}
	return 0;
}