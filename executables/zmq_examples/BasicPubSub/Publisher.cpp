// Basic Publisher
#include <zmq.h>
#include <iostream>
#include <string.h>
#include <unistd.h>

#include "ZMQ_Boilerplate.h"

SET_LOGGING_CFG_NAME("logging.cfg");
ZMQ_Boilerplate boilerplate("Publisher");

int main()
{
	// Socket to publish to listeners
	void *publisher = boilerplate.create_socket(ZMQ_PUB);
	const char *bind_host = "tcp://*:5556";
	LOG4CXX_INFO(boilerplate.getLogger(),"Binding to " << bind_host);
	int rc = zmq_bind(publisher,bind_host);

	LOG4CXX_INFO(boilerplate.getLogger(),"Publishing...");
	uint32_t count = 0;
	while (true)
	{
		zmq_send(publisher,&count,sizeof(count),0);
		LOG4CXX_INFO(boilerplate.getLogger(),"Sent " << count);
		sleep(1);
		count++;
	}
	return 0;
}