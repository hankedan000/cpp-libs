#include <zmq.h>
#include <iostream>
#include <unistd.h>
#include <chrono>

static const char *const usage[] = {
    "zmqping [options] host_name",
    "zmqping [options]",
    NULL,
};

void
log_zmq_error()
{
	std::cerr << "[ERROR]: " << zmq_strerror(zmq_errno()) << std::endl;
}

int
run_pong(
	void *context,
	int port)
{
	int ret_code = 0;

	void *responder = zmq_socket(context,ZMQ_REP);
	char bind_str[128];
	sprintf(bind_str,"tcp://*:%d",port);

	if (zmq_bind(responder,bind_str) == 0)
	{
		std::cout << "PONG responder started..." << std::endl;
	}
	else
	{
		log_zmq_error();
		std::cerr << "attempted to bind to '" << bind_str << "'" << std::endl;
		ret_code = 1;
	}

	char buffer[10];
	while (ret_code == 0)
	{
		int n = zmq_recv(responder,buffer,10,0);
		if (n > 0)
		{
			zmq_send(responder,"pong",4,0);
		}
		else
		{
			log_zmq_error();
		}
	}

	zmq_close(responder);

	return ret_code;
}

int
run_ping(
	void *context,
	const char *host_name,
	int port)
{
	int ret_code = 0;

	void *requester = zmq_socket(context,ZMQ_REQ);
	char bind_str[128];
	sprintf(bind_str,"tcp://%s:%d",host_name,port);

	if (zmq_connect(requester,bind_str) == 0)
	{
		std::cout << "PING " << bind_str << std::endl;
	}
	else
	{
		log_zmq_error();
		std::cerr << "attempted to connect to '" << bind_str << "'" << std::endl;
		ret_code = 1;
	}

	char buffer[10];
	while (ret_code == 0)
	{
		zmq_send(requester,"ping",4,0);
		auto start = std::chrono::high_resolution_clock::now();
		int n = zmq_recv(requester,buffer,10,0);
		if (n > 0)
		{
			auto stop = std::chrono::high_resolution_clock::now();
			auto diff = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
			std::cout << n << " bytes from " << bind_str << ": time=" << diff.count() << " us" << std::endl;
		}
		else
		{
			log_zmq_error();
		}
		sleep(1);
	}

	zmq_close(requester);

	return ret_code;
}

int
main(int argc, const char **argv)
{
	int ret_code = 0;
	bool do_pong = true;
	int daemon = 0;
    const char *host_name = "localhost";
	int port = 5555;

    if (argc > 0)
    {
    	do_pong = false;
    	host_name = argv[0];
    }

	// Socket to talk to requesters
	void *context = zmq_ctx_new();

	if (do_pong)
	{
		int rc = 0;
		if (daemon)
		{
			rc = fork();
		}

		if (rc == 0)
		{
			// if daemonized, only run in child process
			// else, run in parent process per normal
			ret_code = run_pong(context,port);
		}
		else if (rc < 0)
		{
			std::cerr << "Failed to create daemon process!" << std::endl;
		}
		else
		{
			std::cout << "Created pong daemon (PID " << rc << ")" << std::endl;
		}
	}
	else
	{
		ret_code = run_ping(context,host_name,port);
	}

	zmq_ctx_destroy(context);
	return ret_code;
}