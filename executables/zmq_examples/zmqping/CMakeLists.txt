add_executable(zmqping zmqping.cpp)
target_link_libraries(
	zmqping
	${ZMQ_LIBRARIES})