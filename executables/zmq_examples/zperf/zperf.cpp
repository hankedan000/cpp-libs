#include <zmq.h>
#include <iostream>
#include <sched.h>
#include <string>
#include <string.h>
#include <unistd.h>

#include "Reactor.h"
#include "Stopwatch.h"
#include "PingPong.pb.h"
#include "ZMQ_Boilerplate.h"

SET_LOGGING_CFG_NAME("logging.cfg");
ZMQ_Boilerplate boilerplate("zperf");
Reactor reactor;
Stopwatch sw;
void *pub = nullptr;
void *sub = nullptr;
bool stay_alive = true;
uint8_t buffer[500];

zperf::Ping ping_msg;
zperf::Pong pong_msg;

// called by reactor when a ping message was received
void
on_ping(
	const zperf::Ping &datum)
{
	LOG4CXX_TRACE(boilerplate.getLogger(),
		"ping - " << datum.seq());

	pong_msg.set_seq(datum.seq());
	pong_msg.SerializeToArray((void*)buffer,sizeof(buffer));
	size_t samp_size = pong_msg.ByteSizeLong();

	if (zmq_send(pub,(void*)buffer,samp_size,ZMQ_DONTWAIT) < 0)
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			"zmq_send failed for message '" << pong_msg.DebugString() << "'");
	}
}

// called by reactor when a pong message was received
bool firstPong = true;
void
on_pong(
	const zperf::Pong &datum)
{
	if ( ! firstPong)
	{
		sw.stop();
	}
	firstPong = false;

	LOG4CXX_TRACE(boilerplate.getLogger(),
		"pong - " << datum.seq());

	ping_msg.set_seq(datum.seq() + 1);
	ping_msg.SerializeToArray((void*)buffer,sizeof(buffer));
	size_t samp_size = ping_msg.ByteSizeLong();

	sw.start();
	if (zmq_send(pub,(void*)buffer,samp_size,ZMQ_DONTWAIT) < 0)
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			"zmq_send failed for message '" << ping_msg.DebugString() << "'");
	}
}

int main(int argn, const char **argc)
{
	bool okay = true;
	bool doPong = false;
	bool setAffine = false;
	for (unsigned int i=0; i<argn; i++)
	{
		std::string argstr(argc[i]);
		if (argstr == "pong")
		{
			doPong = true;
		}
		else if (argstr == "affine")
		{
			setAffine = true;
		}
	}

	pub = boilerplate.create_socket(ZMQ_PUB);
	sub = boilerplate.create_socket(ZMQ_SUB);
	unsigned int coreAffinity = 0;
	if (doPong)
	{// setup ponger side of application
		coreAffinity = 1;

		if (okay && zmq_connect(sub,"ipc:///tmp/ping.sock") < 0)
		{
			LOG4CXX_ERROR(boilerplate.getLogger(),
				"Failed to connect to ping subscriber!");
			okay = false;
		}
		zmq_setsockopt(sub,ZMQ_SUBSCRIBE,"",0);
		if (okay && zmq_bind(pub,"ipc:///tmp/pong.sock") < 0)
		{
			LOG4CXX_ERROR(boilerplate.getLogger(),
				"Failed to bind to pong publisher!");
			okay = false;
		}
		okay = okay && reactor.add_pb_listener(sub,on_ping);

		if (okay)
		{
			LOG4CXX_INFO(boilerplate.getLogger(),
				"setup pong app!");
		}
	}
	else
	{// setup pinger side of application
		coreAffinity = 0;

		cpu_set_t my_set;        /* Define your cpu_set bit mask. */
		CPU_ZERO(&my_set);       /* Initialize it all to 0, i.e. no CPUs selected. */
		CPU_SET(0, &my_set);     /* set the bit that represents core 0. */
		sched_setaffinity(0, sizeof(cpu_set_t), &my_set);

		if (okay && zmq_connect(sub,"ipc:///tmp/pong.sock") < 0)
		{
			LOG4CXX_ERROR(boilerplate.getLogger(),
				"Failed to connect to pong subscriber!");
			okay = false;
		}
		zmq_setsockopt(sub,ZMQ_SUBSCRIBE,"",0);
		if (okay && zmq_bind(pub,"ipc:///tmp/ping.sock") < 0)
		{
			LOG4CXX_ERROR(boilerplate.getLogger(),
				"Failed to bind to ping publisher!");
			okay = false;
		}
		okay = okay && reactor.add_pb_listener(sub,on_pong);

		if (okay)
		{
			LOG4CXX_INFO(boilerplate.getLogger(),
				"setup ping app!");

			// it's odd, but sending a msg too quickly before context is fully
			// initialized doesn't seem to work. the msg gets dropped.
			usleep(1000000);

			// start the ping pongs
			ping_msg.set_seq(0);
			ping_msg.SerializeToArray((void*)buffer,sizeof(buffer));
			size_t samp_size = ping_msg.ByteSizeLong();

			if (zmq_send(pub,(void*)buffer,samp_size,ZMQ_DONTWAIT) < 0)
			{
				LOG4CXX_ERROR(boilerplate.getLogger(),
					"zmq_send failed for message '" << ping_msg.DebugString() << "'");
			}
		}
	}

	if (setAffine)
	{
		cpu_set_t my_set;              /* Define your cpu_set bit mask. */
		CPU_ZERO(&my_set);             /* Initialize it all to 0, i.e. no CPUs selected. */
		CPU_SET(coreAffinity, &my_set);/* set the bit that represents core. */
		if (sched_setaffinity(0, sizeof(cpu_set_t), &my_set) < 0)
		{
			LOG4CXX_ERROR(boilerplate.getLogger(),
				"failed to set core affinity to " << coreAffinity << "."
				" err = " << strerror(errno));
			okay = false;
		}
		else
		{
			LOG4CXX_INFO(boilerplate.getLogger(),
				"set core affinity to " << coreAffinity << ".");
		}
	}

	// wait for messages to arrive
	if (okay)
	{
		LOG4CXX_INFO(boilerplate.getLogger(),
			"reactor is listening...");
		if ( ! reactor.start())
		{
			LOG4CXX_ERROR(boilerplate.getLogger(),
				"reactor failed to start!");
		}
		else
		{
			LOG4CXX_INFO(boilerplate.getLogger(),
				"reactor shutdown complete!");
		}
	}

	if ( ! doPong)
	{
		LOG4CXX_INFO(boilerplate.getLogger(),
			__func__ << " - \n" << sw.getSummary());
	}

	return okay ? 0 : 1;
}