// Hello World Requester
#include <zmq.h>
#include <iostream>
#include <unistd.h>

int main()
{
	std::cout << "Connecting to Helloworld responder..." << std::endl;
	// Socket to talk to responder
	void *context = zmq_ctx_new();
	void *requester = zmq_socket(context,ZMQ_REQ);
	zmq_connect(requester,"tcp://localhost:5555");

	for (unsigned int i=0; i<10; i++)
	{
		char buffer[10];
		std::cout << "Sending Hello " << i << "..." << std::endl;
		zmq_send(requester,"Hello",5,0);
		zmq_recv(requester,buffer,10,0);
		std::cout << "Received World " << i << "!" << std::endl;
	}
	zmq_close(requester);
	zmq_ctx_destroy(context);
	return 0;
}