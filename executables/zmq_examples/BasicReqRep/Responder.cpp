// Hello World Responder
#include <zmq.h>
#include <iostream>
#include <unistd.h>

int main()
{
	// Socket to talk to requesters
	void *context = zmq_ctx_new();
	void *responder = zmq_socket(context,ZMQ_REP);
	int rc = zmq_bind(responder,"tcp://*:5555");

	std::cout << "Responder is listening..." << std::endl;
	char buffer[10];
	while (rc == 0)
	{
		zmq_recv(responder,buffer,10,0);
		std::cout << "Received Hello" << std::endl;
		sleep(1);
		zmq_send(responder,"World",5,0);
	}
	return 0;
}