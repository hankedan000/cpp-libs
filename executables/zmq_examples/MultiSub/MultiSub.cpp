#include "Boilerplate.h"
#include <zmq.h>

Boilerplate boilerplate("MultiSub");

void child1()
{
	void *context = zmq_ctx_new();
	void *publisher = zmq_socket(context,ZMQ_PUB);
	const char *bind_host = "tcp://*:5557";
	LOG4CXX_INFO(boilerplate.getLogger(),
		__func__ << " - " <<
		"Binding to " << bind_host);
	int rc = zmq_bind(publisher,bind_host);

	uint32_t count = 0;
	while (true)
	{
		zmq_send(publisher,&count,sizeof(count),0);
		LOG4CXX_INFO(boilerplate.getLogger(),
			__func__ << " - "
			"Sent " << count);
		sleep(1);
		count++;
	}
}

void child2()
{
	void *context = zmq_ctx_new();
	void *publisher = zmq_socket(context,ZMQ_PUB);
	const char *bind_host = "tcp://*:5558";
	LOG4CXX_INFO(boilerplate.getLogger(),
		__func__ << " - " <<
		"Binding to " << bind_host);
	int rc = zmq_bind(publisher,bind_host);

	uint32_t count = 0;
	while (true)
	{
		zmq_send(publisher,&count,sizeof(count),0);
		LOG4CXX_INFO(boilerplate.getLogger(),
			__func__ << " - "
			"Sent " << count);
		sleep(2);
		count++;
	}
}

int main()
{
	LOG4CXX_INFO(boilerplate.getLogger(),
		"Spawning child1...");
	pid_t pid = boilerplate.fork(false);
	if (pid == 0)
	{
		child1();
	}
	else if (pid < 0)
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			"Failed to spawn child1!");
		exit(EXIT_FAILURE);
	}

	LOG4CXX_INFO(boilerplate.getLogger(),
		"Spawning child2...");
	pid = boilerplate.fork(false);
	if (pid == 0)
	{
		child2();
	}
	else if (pid < 0)
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			"Failed to spawn child2!");
		exit(EXIT_FAILURE);
	}

	void *context = zmq_ctx_new();
	void *sub1 = zmq_socket(context,ZMQ_SUB);
	void *sub2 = zmq_socket(context,ZMQ_SUB);
	int rc = zmq_connect(sub1,"tcp://localhost:5557");
	if (rc < 0)
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			"Failed to connect to sub1!");
		exit(EXIT_FAILURE);
	}
	rc = zmq_connect(sub2,"tcp://localhost:5558");
	if (rc < 0)
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			"Failed to connect to sub2!");
		exit(EXIT_FAILURE);
	}
	rc = zmq_setsockopt(sub1,ZMQ_SUBSCRIBE,"",0);
	rc = zmq_setsockopt(sub2,ZMQ_SUBSCRIBE,"",0);

	LOG4CXX_INFO(boilerplate.getLogger(),
		"Listening to children...");
	zmq_pollitem_t items [2];
	/* First item refers to ØMQ socket 'socket' */
	items[0].socket = sub1;
	items[0].events = ZMQ_POLLIN;
	/* Second item refers to standard socket 'fd' */
	items[1].socket = sub2;
	items[1].events = ZMQ_POLLIN;
	while (true)
	{
		/* Poll for events indefinitely */
		int rc = zmq_poll(items, 2, -1);
		if (rc < 0)
		{
			LOG4CXX_ERROR(boilerplate.getLogger(),
				__func__ << " - "
				"zmq_poll() failed! error = " << zmq_strerror(errno));
		}
		else
		{
			for (unsigned int i=0; i<2; i++)
			{
				if (items[i].revents & ZMQ_POLLIN)
				{
					uint32_t count = 0;
					zmq_recv(items[i].socket,&count,sizeof(count),0);
					LOG4CXX_INFO(boilerplate.getLogger(),
						__func__ << " - "
						"Received " << count << " for sub" << (i+1));
				}
			}
		}
	}

	return 0;
}