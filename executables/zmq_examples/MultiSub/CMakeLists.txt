add_executable(MultiSub MultiSub.cpp)
target_link_libraries(MultiSub
	Boilerplate
	${ZMQ_LIBRARIES})
