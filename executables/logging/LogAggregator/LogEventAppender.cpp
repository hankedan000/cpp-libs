#include "LogEventAppender.h"
#include <log4cxx/helpers/loglog.h>
#include <log4cxx/helpers/outputstreamwriter.h>
#include <log4cxx/helpers/charsetencoder.h>
#include <log4cxx/helpers/optionconverter.h>
#include <log4cxx/helpers/stringhelper.h>
#include <log4cxx/xml/xmllayout.h>
#include <log4cxx/level.h>
#include <log4cxx/helpers/transform.h>
#include <log4cxx/helpers/synchronized.h>
#include <log4cxx/helpers/transcoder.h>
#include <log4cxx/helpers/socketoutputstream.h>

using namespace log4cxx;
using namespace log4cxx::helpers;
using namespace log4cxx::net;
using namespace log4cxx::xml;

IMPLEMENT_LOG4CXX_OBJECT(LogEventAppender)

// The default port number of remote logging server (4560)
int LogEventAppender::DEFAULT_PORT                 = 4560;

// The default reconnection delay (30000 milliseconds or 30 seconds).
int LogEventAppender::DEFAULT_RECONNECTION_DELAY   = 30000;

const int LogEventAppender::MAX_EVENT_LEN          = 1024;

LogEventAppender::LogEventAppender()
	: SocketAppenderSkeleton(DEFAULT_PORT, DEFAULT_RECONNECTION_DELAY)
{
	layout = new XMLLayout();
}

LogEventAppender::LogEventAppender(InetAddressPtr address1, int port1)
	: SocketAppenderSkeleton(address1, port1, DEFAULT_RECONNECTION_DELAY)
{
	layout = new XMLLayout();
	Pool p;
	activateOptions(p);
}

LogEventAppender::LogEventAppender(const LogString& host, int port1)
	: SocketAppenderSkeleton(host, port1, DEFAULT_RECONNECTION_DELAY)
{
	layout = new XMLLayout();
	Pool p;
	activateOptions(p);
}

LogEventAppender::~LogEventAppender()
{
	finalize();
}

void LogEventAppender::appendLogEvent(const LogEvent& event, log4cxx::helpers::Pool& p)
{
	if (writer != 0)
	{
		LogString output;
		formatXML(output, event, p);

		try
		{
			writer->write(output, p);
			writer->flush(p);
		}
		catch (std::exception& e)
		{
			writer = 0;
			LogLog::warn(LOG4CXX_STR("Detected problem with connection: "), e);

			if (getReconnectionDelay() > 0)
			{
				fireConnector();
			}
		}
	}
}


int LogEventAppender::getDefaultDelay() const
{
	return DEFAULT_RECONNECTION_DELAY;
}

int LogEventAppender::getDefaultPort() const
{
	return DEFAULT_PORT;
}

void LogEventAppender::setSocket(log4cxx::helpers::SocketPtr& socket, Pool& p)
{
	OutputStreamPtr os(new SocketOutputStream(socket));
	CharsetEncoderPtr charset(CharsetEncoder::getUTF8Encoder());
	synchronized sync(mutex);
	writer = new OutputStreamWriter(os, charset);
}

void LogEventAppender::cleanUp(Pool& p)
{
	if (writer != 0)
	{
		try
		{
			writer->close(p);
			writer = 0;
		}
		catch (std::exception&)
		{
		}
	}
}

void LogEventAppender::append(const spi::LoggingEventPtr& event, log4cxx::helpers::Pool& p)
{
	if (writer != 0)
	{
		LogString output;
		layout->format(output, event, p);

		try
		{
			writer->write(output, p);
			writer->flush(p);
		}
		catch (std::exception& e)
		{
			writer = 0;
			LogLog::warn(LOG4CXX_STR("Detected problem with connection: "), e);

			if (getReconnectionDelay() > 0)
			{
				fireConnector();
			}
		}
	}
}

log4cxx::LevelPtr
LogEventAppender::convertLevel(
	const LogEvent_Level &level) const
{
	switch (level)
	{
		case LogEvent_Level_FATAL:
			return log4cxx::Level::getFatal();
		case LogEvent_Level_ERROR:
			return log4cxx::Level::getError();
		case LogEvent_Level_WARN:
			return log4cxx::Level::getWarn();
		case LogEvent_Level_INFO:
			return log4cxx::Level::getInfo();
		case LogEvent_Level_DEBUG:
			return log4cxx::Level::getDebug();
		case LogEvent_Level_TRACE:
			return log4cxx::Level::getTrace();
		default:
			LogLog::warn(LOG4CXX_STR(
				"Converting unsupported level (" + std::to_string((int)level) + ") to WARN"));
			return log4cxx::Level::getWarn();
	}
}

void
LogEventAppender::formatXML(
	log4cxx::LogString& output,
	const LogEvent &event,
	log4cxx::helpers::Pool& p) const
{
	using namespace log4cxx;
	using namespace log4cxx::helpers;
	using namespace log4cxx::spi;

	output.append(LOG4CXX_STR("<log4j:event logger=\""));
	Transform::appendEscapingTags(output, event.logger());
	output.append(LOG4CXX_STR("\" timestamp=\""));
	StringHelper::toString((log4cxx_int64_t)(event.timestamp() / 1000L), p, output);
	output.append(LOG4CXX_STR("\" level=\""));
	Transform::appendEscapingTags(output, convertLevel(event.level())->toString());
	output.append(LOG4CXX_STR("\" thread=\""));
	Transform::appendEscapingTags(output, event.thread_name());
	output.append(LOG4CXX_STR("\">"));
	output.append(LOG4CXX_EOL);

	output.append(LOG4CXX_STR("<log4j:message><![CDATA["));
	// Append the rendered message. Also make sure to escape any
	// existing CDATA sections.
	Transform::appendEscapingCDATA(output, event.msg());
	output.append(LOG4CXX_STR("]]></log4j:message>"));
	output.append(LOG4CXX_EOL);

#if 0
	LogString ndc;

	if (event->getNDC(ndc))
	{
		output.append(LOG4CXX_STR("<log4j:NDC><![CDATA["));
		Transform::appendEscapingCDATA(output, ndc);
		output.append(LOG4CXX_STR("]]></log4j:NDC>"));
		output.append(LOG4CXX_EOL);
	}
#endif

	if (event.has_location_info())
	{
		output.append(LOG4CXX_STR("<log4j:locationInfo class=\""));
		const LogEvent::LocationInfo &locInfo = event.location_info();
		LOG4CXX_DECODE_CHAR(className, locInfo.class_name());
		Transform::appendEscapingTags(output, className);
		output.append(LOG4CXX_STR("\" method=\""));
		LOG4CXX_DECODE_CHAR(method, locInfo.method_name());
		Transform::appendEscapingTags(output, method);
		output.append(LOG4CXX_STR("\" file=\""));
		LOG4CXX_DECODE_CHAR(fileName, locInfo.file_name());
		Transform::appendEscapingTags(output, fileName);
		output.append(LOG4CXX_STR("\" line=\""));
		StringHelper::toString(locInfo.line_num(), p, output);
		output.append(LOG4CXX_STR("\"/>"));
		output.append(LOG4CXX_EOL);
	}

#if 0
	if (properties)
	{
		LoggingEvent::KeySet propertySet(event->getPropertyKeySet());
		LoggingEvent::KeySet keySet(event->getMDCKeySet());

		if (!(keySet.empty() && propertySet.empty()))
		{
			output.append(LOG4CXX_STR("<log4j:properties>"));
			output.append(LOG4CXX_EOL);

			for (LoggingEvent::KeySet::const_iterator i = keySet.begin();
				i != keySet.end();
				i++)
			{
				LogString key(*i);
				LogString value;

				if (event->getMDC(key, value))
				{
					output.append(LOG4CXX_STR("<log4j:data name=\""));
					Transform::appendEscapingTags(output, key);
					output.append(LOG4CXX_STR("\" value=\""));
					Transform::appendEscapingTags(output, value);
					output.append(LOG4CXX_STR("\"/>"));
					output.append(LOG4CXX_EOL);
				}
			}

			for (LoggingEvent::KeySet::const_iterator i2 = propertySet.begin();
				i2 != propertySet.end();
				i2++)
			{
				LogString key(*i2);
				LogString value;

				if (event->getProperty(key, value))
				{
					output.append(LOG4CXX_STR("<log4j:data name=\""));
					Transform::appendEscapingTags(output, key);
					output.append(LOG4CXX_STR("\" value=\""));
					Transform::appendEscapingTags(output, value);
					output.append(LOG4CXX_STR("\"/>"));
					output.append(LOG4CXX_EOL);
				}
			}

			output.append(LOG4CXX_STR("</log4j:properties>"));
			output.append(LOG4CXX_EOL);
		}
	}
#endif

	output.append(LOG4CXX_STR("</log4j:event>"));
	output.append(LOG4CXX_EOL);
	output.append(LOG4CXX_EOL);
}
