#ifndef _LOGAGGREGATOR_LOG_EVENT_APPENDER_H
#define _LOGAGGREGATOR_LOG_EVENT_APPENDER_H

#include <log4cxx/net/socketappenderskeleton.h>
#include <log4cxx/helpers/writer.h>
#include "LogEvent.pb.h"

/**
Sends {@link log4cxx::spi::LoggingEvent LoggingEvent} objects in XML format
        to a remote a log server, usually a XMLSocketNode.

<p>The LogEventAppender has the following properties:

- If sent to a XMLSocketNode, remote logging
        is non-intrusive as far as the log event is concerned. In other
words, the event will be logged with the same time stamp, {@link
NDC NDC}, location info as if it were logged locally by
the client.

- LogEventAppenders use exclusively an XMLLayout. They ship an
XML stream representing a {@link spi::LoggingEvent LoggingEvent} object
        to the server side.

- Remote logging uses the TCP protocol. Consequently, if
the server is reachable, then log events will eventually arrive
at the server.

- If the remote server is down, the logging requests are
simply dropped. However, if and when the server comes back up,
then event transmission is resumed transparently. This
transparent reconneciton is performed by a <em>connector</em>
thread which periodically attempts to connect to the server.

- Logging events are automatically <em>buffered</em> by the
native TCP implementation. This means that if the link to server
is slow but still faster than the rate of (log) event production
by the client, the client will not be affected by the slow
network connection. However, if the network connection is slower
then the rate of event production, then the client can only
progress at the network rate. In particular, if the network link
to the the server is down, the client will be blocked.
@n @n On the other hand, if the network link is up, but the server
is down, the client will not be blocked when making log requests
but the log events will be lost due to server unavailability.

- Even if an <code>LogEventAppender</code> is no longer
attached to any logger, it will not be destroyed in
the presence of a connector thread. A connector thread exists
only if the connection to the server is down. To avoid this
destruction problem, you should #close the the
<code>LogEventAppender</code> explicitly. See also next item.
@n @n Long lived applications which create/destroy many
<code>LogEventAppender</code> instances should be aware of this
destruction problem. Most other applications can safely
ignore it.

- If the application hosting the <code>LogEventAppender</code>
        exits before the <code>LogEventAppender</code> is closed either
explicitly or subsequent to destruction, then there might
be untransmitted data in the pipe which might be lost.
@n @n To avoid lost data, it is usually sufficient to
#close the <code>LogEventAppender</code> either explicitly or by
calling the LogManager#shutdown method
before exiting the application.
*/

namespace log4cxx
{
namespace net
{

class LOG4CXX_EXPORT LogEventAppender : public SocketAppenderSkeleton
{
	public:
		/**
		The default port number of remote logging server (4560).
		*/
		static int DEFAULT_PORT;

		/**
		The default reconnection delay (30000 milliseconds or 30 seconds).
		*/
		static int DEFAULT_RECONNECTION_DELAY;

		/**
		An event XML stream cannot exceed 1024 bytes.
		*/
		static const int MAX_EVENT_LEN;

		DECLARE_LOG4CXX_OBJECT(LogEventAppender)
		BEGIN_LOG4CXX_CAST_MAP()
		LOG4CXX_CAST_ENTRY(LogEventAppender)
		LOG4CXX_CAST_ENTRY_CHAIN(AppenderSkeleton)
		END_LOG4CXX_CAST_MAP()

		LogEventAppender();
		~LogEventAppender();

		/**
		Connects to remote server at <code>address</code> and <code>port</code>.
		*/
		LogEventAppender(helpers::InetAddressPtr address, int port);

		/**
		Connects to remote server at <code>host</code> and <code>port</code>.
		*/
		LogEventAppender(const LogString& host, int port);

		void appendLogEvent(const LogEvent& event, log4cxx::helpers::Pool& p);


	protected:
		virtual void setSocket(log4cxx::helpers::SocketPtr& socket, log4cxx::helpers::Pool& p);

		virtual void cleanUp(log4cxx::helpers::Pool& p);

		virtual int getDefaultDelay() const;

		virtual int getDefaultPort() const;

		void append(const spi::LoggingEventPtr& event, log4cxx::helpers::Pool& pool);

		log4cxx::LevelPtr
		convertLevel(
			const LogEvent_Level &level) const;

		void
		formatXML(
			log4cxx::LogString& output,
			const LogEvent &event,
			log4cxx::helpers::Pool& p) const;

	private:
		log4cxx::helpers::WriterPtr writer;
		//  prevent copy and assignment statements
		LogEventAppender(const LogEventAppender&);
		LogEventAppender& operator=(const LogEventAppender&);
}; // class LogEventAppender

LOG4CXX_PTR_DEF(LogEventAppender);

} // namespace net
} // namespace log4cxx

#endif // _LOG4CXX_NET_XML_SOCKET_APPENDER_H

