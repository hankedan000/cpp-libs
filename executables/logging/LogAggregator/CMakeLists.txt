add_executable(
	LogAggregator
		LogAggregator.cpp
		LogEventAppender.cpp
)
target_link_libraries(
	LogAggregator
		${JsonCpp_LIBRARIES}
		log4cxx
		Logging
		LogEvent_pb_static
		Stopwatch
		${ZMQ_LIBRARIES}
)
install(
	TARGETS LogAggregator
	RUNTIME
		DESTINATION bin
)
install(
	FILES "config/LogAggregatorConfig.json"
	DESTINATION "etc/default"
)
install(
	FILES "scripts/laggrd.service"
	DESTINATION "/etc/systemd/system"
)
