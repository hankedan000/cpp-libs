#include <iostream>
#include <fstream>
#include <string.h>
#include <unistd.h>
#include <csignal>
#include <sys/stat.h>

#include "LogEvent.pb.h"
#include "LogEventAppender.h"
#include "Stopwatch.h"
#include "ZMQ_Appender.h"

// include json library
#include <json/json.h>

// include log4cxx header files
#include <log4cxx/logger.h>
#include <log4cxx/basicconfigurator.h>
#include <log4cxx/helpers/loglog.h>
#include <log4cxx/helpers/stringhelper.h>
#include <log4cxx/helpers/transform.h>
#include <log4cxx/net/socketappender.h>

// include ZeroMQ headers
#include <zmq.h>

struct Config
{
	Config()
	{
		setDefaults();
	}

	void
	setDefaults()
	{
		loggingThreshold = "INFO";
		remoteHost = "localhost";
		port = 4448;
		reconnectDelay = 1000;
		locationInfo = true;
		swEnabled = false;
		swSummaryInterval = 1000;
	}

	std::string loggingThreshold;
	std::string remoteHost;
	unsigned int port;
	unsigned int reconnectDelay;
	bool locationInfo;
	bool swEnabled;
	unsigned int swSummaryInterval;
};

log4cxx::LoggerPtr logger(log4cxx::Logger::getLogger("LogAggregator"));
Stopwatch sw;
bool stay_alive = true;
Config cfg;

void
handle_sigint(
	int)
{
	LOG4CXX_WARN(logger,"Requested shutdown...");
	stay_alive = false;
}

bool
parseConfig()
{
	bool okay = true;
	const std::string FILENAME = "LogAggregatorConfig.json";
	const std::string USER_CFG_PATH = "/usr/local/etc/";
	const std::string DEFAULT_CFG_PATH = "/usr/local/etc/default/";
	std::string cfg_path(USER_CFG_PATH + FILENAME);

	Json::Value root;
	std::ifstream ifs(cfg_path);

	if ( ! ifs.good())
	{
		LOG4CXX_WARN(logger,
			"'" << cfg_path << "'' doesn't exist. Trying default config path.");
		cfg_path = DEFAULT_CFG_PATH + FILENAME;
		ifs.open(cfg_path);
	}
	if ( ! ifs.good())
	{
		LOG4CXX_WARN(logger,
			"'" << cfg_path << "'' doesn't exist. Trying locally.");
		cfg_path = FILENAME;
		ifs.open(cfg_path);
	}
	if ( ! ifs.good())
	{
		LOG4CXX_ERROR(logger,
			"Could not find '" << FILENAME << "'.");
		okay = false;
	}

	Json::CharReaderBuilder builder;
	JSONCPP_STRING errs;
	if ( okay && ! parseFromStream(builder,ifs,&root,&errs))
	{
		LOG4CXX_ERROR(logger,
			"Failed to parse '" << FILENAME << "'. " << errs);
		okay = false;
	}

	if (okay)
	{
		cfg.loggingThreshold = root["logging_threshold"].asString();
		cfg.remoteHost = root["remote_host"].asString();
		cfg.port = root["port"].asUInt();
		cfg.reconnectDelay = root["reconnect_delay"].asUInt();
		cfg.locationInfo = root["location_info"].asBool();
		cfg.swEnabled = root["sw_enabled"].asBool();
		cfg.swSummaryInterval = root["sw_summary_interval"].asUInt();
	}

	return okay;
}

bool
aggregate(
	log4cxx::net::LogEventAppender *appender)
{
	bool okay = true;
	log4cxx::helpers::Pool pool;

	// Socket to listen to publishers
	void *context = zmq_ctx_new();
	void *subscriber = zmq_socket(context,ZMQ_SUB);
	const std::string SOCK_PATH = "/var/run/zmq_appender_log_event.sock";
	const std::string HOST = "ipc://" + SOCK_PATH;
	if (okay && zmq_bind(subscriber,HOST.c_str()) < 0)
	{
		LOG4CXX_ERROR(logger,"Failed to bind socket to \"" << HOST << "\"");
		okay = false;
	}
	if (okay && zmq_setsockopt(subscriber,ZMQ_SUBSCRIBE,"",0) < 0)
	{
		LOG4CXX_ERROR(logger,"Failed to create subscriber");
		okay = false;
	}
	if (okay)
	{
		// allow all users to connect and log events to socket
		chmod(SOCK_PATH.c_str(),0777);
	}

	// allocate a buffer for receiving message from appender
	const size_t BUFFER_SIZE = ZMQ_Appender::MAX_BUFFER_SIZE;
	void *buffer = malloc(BUFFER_SIZE);

	if (okay)
	{
		LOG4CXX_INFO(logger,"Listening to publisher...");
		while (stay_alive)
		{
			int rbytes = zmq_recv(subscriber,buffer,BUFFER_SIZE,0);
			LOG4CXX_DEBUG(logger,"received " << rbytes << " bytes");

			sw.start();
			if (rbytes < 0)
			{
				LOG4CXX_ERROR(logger,"zmq_recv failed");
				continue;
			}
			else if (rbytes > BUFFER_SIZE)
			{
				LOG4CXX_ERROR(logger,"zmq_recv message was truncated!");
				continue;
			}

			// deserialize LogEvent
			LogEvent datum;
			if ( ! datum.ParseFromArray(buffer,rbytes))
			{
				LOG4CXX_ERROR(logger,"datum.ParseFromArray() failed!");
				continue;
			}

			LOG4CXX_DEBUG(logger,
				"logger: " << datum.logger() << "; "
				"msg: "    << datum.msg()    << "; ");

			// send log to Chainsaw
			appender->appendLogEvent(datum,pool);
			sw.stop();

			if (sw.enabled() &&
				sw.intervals() > 0 &&
				sw.intervals() % cfg.swSummaryInterval == 0)
			{
				LOG4CXX_INFO(logger,sw.getSummary());
			}
		}
	}
	
	zmq_close(subscriber);
	zmq_ctx_destroy(context);

	if (buffer != nullptr)
	{
		free(buffer);
	}

	return okay;
}

static
void
skeleton_daemon()
{
	pid_t pid;

	/* Fork off the parent process */
	pid = fork();

	/* An error occurred */
	if (pid < 0)
		exit(EXIT_FAILURE);

	/* Success: Let the parent terminate */
	if (pid > 0)
		exit(EXIT_SUCCESS);

	/* On success: The child process becomes session leader */
	if (setsid() < 0)
		exit(EXIT_FAILURE);
}

int
main(
	int argc,
	char **argv)
{
	bool daemonize = false;
	for (unsigned int i=0; i<argc; i++)
	{
		if (strcmp(argv[i],"--daemon") == 0)
		{
			daemonize = true;
		}
	}

	if (daemonize)
	{
		// fork to a daemon process
		skeleton_daemon();
	}

	signal(SIGINT,handle_sigint);
	// setup a simple configuration that logs to console
	log4cxx::BasicConfigurator::configure();

	/**
	 * enable internal log4cxx debug logging
	 * This is useful for debugging the SocketAppender class
	 * that our LogEventAppender inherits from.
	 */
	log4cxx::helpers::LogLog::setInternalDebugging(false);

	if ( ! parseConfig())
	{
		LOG4CXX_WARN(logger,
			"An error occured while parsing config options; using defaults.");
		cfg.setDefaults();
	}

	// set logging level based on config
	log4cxx::LevelPtr threshold = Level::toLevel(
		cfg.loggingThreshold,
		log4cxx::Level::getInfo());
	logger->setLevel(threshold);

	// configure stopwatch
	sw.enabled(cfg.swEnabled);

	// configure chainsaw appender
	LOG4CXX_INFO(logger,
		"Connecting appender to " << cfg.remoteHost << ":" << cfg.port);
	log4cxx::net::LogEventAppender appender(cfg.remoteHost,cfg.port);
	appender.setReconnectionDelay(cfg.reconnectDelay);

	// start aggregating logs on this node
	if ( ! aggregate(&appender))
	{
		LOG4CXX_ERROR(logger,"failed to aggregate logs!");
	}

	LOG4CXX_INFO(logger,"shutting down appender...");
	appender.close();

	return 0;
}