#include <iostream>
#include <string.h>
#include <unistd.h>
#include <csignal>
#include "Boilerplate.h"
#include "Stopwatch.h"

#include "Logging.h"

SET_LOGGING_CFG_NAME("logging.cfg");

Boilerplate boilerplate("BasicLogger");
bool stay_alive = true;
bool parent = true;
Stopwatch sw;

void handle_sigint(int)
{
	LOG4CXX_WARN(boilerplate.getLogger(),"Requested shutdown...");
	stay_alive = false;
}

int main(int argc, char **argv)
{
	signal(SIGINT,handle_sigint);
	bool daemonize = false;
	for (unsigned int i=0; i<argc; i++)
	{
		if (strcmp(argv[i],"--daemon") == 0)
		{
			daemonize = true;
		}
	}

	LOG4CXX_TRACE(boilerplate.getLogger(),"This is a TRACE level log");
	LOG4CXX_DEBUG(boilerplate.getLogger(),"This is a DEBUG level log");
	LOG4CXX_INFO(boilerplate.getLogger(),"This is an INFO level log");
	LOG4CXX_WARN(boilerplate.getLogger(),"This is a WARN level log");
	LOG4CXX_ERROR(boilerplate.getLogger(),"This is an ERROR level log");
	LOG4CXX_FATAL(boilerplate.getLogger(),"This is a FATAL level log");

	if (daemonize)
	{
		LOG4CXX_INFO(boilerplate.getLogger(),"Forking to daemon process...");
		// fork to a daemon process
		parent = boilerplate.fork(true) > 0;
	}

	LOG4CXX_DEBUG(boilerplate.getLogger(),"----- BEGIN Logging -----");
	unsigned int count = 0;
	while (stay_alive)
	{
		sw.start();
		LOG4CXX_INFO(boilerplate.getLogger(),(parent ? "P: " : "C: ") << "Sent " << count);
		sw.stop();
		usleep(1000000);
		count++;
	}
	LOG4CXX_DEBUG(boilerplate.getLogger(),"------ END Logging ------");
	LOG4CXX_INFO(boilerplate.getLogger(),sw.getSummary());

	return 0;
}