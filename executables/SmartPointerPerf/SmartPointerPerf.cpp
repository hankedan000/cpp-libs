#include <iostream>
#include <memory>
#include "Stopwatch.h"
#include "tqdm.h"

struct MyStruct
{
	unsigned int coolNum;
	bool coolBool;
};

int main()
{
	const unsigned int NSAMPS = 10000000;
	tqdm bar;

	std::cout << "new & delete" << std::endl;
	std::cout << "ptr" << std::endl;
	MyStruct *ptr = 0;
	for (unsigned int i=0; i<NSAMPS; i++)
	{
		bar.progress(i,NSAMPS);
		ptr = new MyStruct;
		ptr->coolNum = 0;
		delete ptr;
	}
	std::cout << std::endl << "std::shared_ptr" << std::endl;
	for (unsigned int i=0; i<NSAMPS; i++)
	{
		bar.progress(i,NSAMPS);
		std::shared_ptr<MyStruct> sptr = std::make_shared<MyStruct>();
		sptr->coolNum = 0;
	}
	std::cout << std::endl << "std::weak_ptr" << std::endl;
	std::shared_ptr<MyStruct> ref_me = std::make_shared<MyStruct>();
	for (unsigned int i=0; i<NSAMPS; i++)
	{
		bar.progress(i,NSAMPS);
		std::weak_ptr<MyStruct> wptr = ref_me;
		wptr.lock()->coolNum = 0;
	}

	std::cout << std::endl << "Done!" << std::endl;

	return 0;
}