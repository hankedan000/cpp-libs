#include <iostream>
#include "tqdm.h"
#include <tuple>
#include <set>

class SortMe
{
public:
	SortMe(
		unsigned int a,
		unsigned int b,
		unsigned int c,
		bool d)
	 : a(a)
	 , b(b)
	 , c(c)
	 , d(d)
	{}

	unsigned int a;
	unsigned int b;
	unsigned int c;
	bool d;
};

#if 0
bool
operator<(
	const SortMe &lhs,
	const SortMe &rhs)
{
	return std::tie(lhs.a,lhs.b,lhs.c,lhs.d)
		<
		std::tie(rhs.a,rhs.b,rhs.c,rhs.d);
}
#else
bool
operator<(
	const SortMe &lhs,
	const SortMe &rhs)
{
	if (lhs.a < rhs.a)
	{
		return true;
	}
	else if (lhs.a == rhs.a)
	{
		if (lhs.b < rhs.b)
		{
			return true;
		}
		else if (lhs.b == rhs.b)
		{
			if (lhs.c < rhs.c)
			{
				return true;
			}
			else if (lhs.c == rhs.c)
			{
				if (lhs.d < rhs.d)
				{
					return true;
				}
			}
		}
	}
	return false;
}
#endif

int main()
{
	const unsigned int MAX_A = 100;
	const unsigned int MAX_B = 200;
	const unsigned int MAX_C = 200;
	const unsigned int MAX_D = 100;
	tqdm bar;
	unsigned int i=0;
	printf("Hello!\n");
	std::set<SortMe> container;
	for (unsigned int a=0; a<MAX_A; a++)
	{
		for (unsigned int b=0; b<MAX_B; b++)
		{
			for (unsigned int c=0; c<MAX_C; c++)
			{
				for (unsigned int d=0; d<MAX_D; d++)
				{
					container.emplace(a,b,c,d%4);
					bar.progress(i++,MAX_A*MAX_B*MAX_C*MAX_D);
				}
			}
		}
	}
	bar.finish();

	return 0;
}