#include <iostream>

#define TRACEPOINT_DEFINE
#include "basic_tracepoints.h"

void funcC(int id)
{
	tracepoint(basic_ust, event_c, id);
}

void funcB(int id)
{
	tracepoint(basic_ust, event_b, id);
	for (unsigned int i=0; i<id; i++)
	{
		funcC(id);
	}
}

void funcA(int id)
{
	tracepoint(basic_ust, event_a, id);
	for (unsigned int i=0; i<5; i++)
	{
		funcB(id);
	}
}

int main()
{
	std::cout << "Helloworld!" << std::endl;

	while (true)
	{
		for (int id=0; id<10; id++)
		{
			funcA(id);
		}
	}

	return 0;
}