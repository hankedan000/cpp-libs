#undef TRACEPOINT_PROVIDER
#define TRACEPOINT_PROVIDER basic_ust

#undef TRACEPOINT_INCLUDE
#define TRACEPOINT_INCLUDE "./basic_tracepoints.h"

#if !defined(_TP_H) || defined(TRACEPOINT_HEADER_MULTI_READ)
#define _TP_H

#include <lttng/tracepoint.h>

/* The tracepoint class */
TRACEPOINT_EVENT_CLASS(
	/* Tracepoint provider name */
	TRACEPOINT_PROVIDER,

	/* Tracepoint class name */
	basic_event_class,

	/* Input arguments */
	TP_ARGS(
		int, id
	),

	/* Output event fields */
	TP_FIELDS(
		ctf_integer(int, id, id)
	)
)

/* The tracepoint instances */
TRACEPOINT_EVENT_INSTANCE(
	/* Tracepoint provider name */
	TRACEPOINT_PROVIDER,

	/* Tracepoint class name */
	basic_event_class,

	/* Tracepoint name */
	event_a,

	/* Input arguments */
	TP_ARGS(
		int, id
	)
)
TRACEPOINT_EVENT_INSTANCE(
	TRACEPOINT_PROVIDER,
	basic_event_class,
	event_b,
	TP_ARGS(
		int, id
	)
)
TRACEPOINT_EVENT_INSTANCE(
	TRACEPOINT_PROVIDER,
	basic_event_class,
	event_c,
	TP_ARGS(
		int, id
	)
)

#endif /* _TP_H */

#include <lttng/tracepoint-event.h>
