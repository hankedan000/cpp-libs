include_directories(${CMAKE_CURRENT_SOURCE_DIR})
add_library(basic_tracepoints
	basic_tracepoints.c)
target_link_libraries(basic_tracepoints LTTng::UST)

add_executable(basic_ust
	basic_ust.cpp)
target_link_libraries(basic_ust basic_tracepoints)